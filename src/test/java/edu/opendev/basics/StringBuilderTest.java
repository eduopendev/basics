package edu.opendev.basics;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Тест о пользе StringBuilder и о време прямой конкатенации строк
 * Created by ralex on 13.09.16.
 */
public class StringBuilderTest {

    private long start;
    private long end;
    private int N = 50000;

    @Before
    public void before() {
        start = System.nanoTime();
    }

    @After
    public void after() {
        end = System.nanoTime();
        System.out.println("time " + (end - start)/1000000. + " msc");
        System.out.printf("%n");
    }

    @Test
    public void testDirectConcatenate() {
        String s = "1";
        for(int i=0; i<N; i++) {
            s += "+" + i;
        }
    }

    @Test
    public void testStringBuilder1() {
        StringBuilder sb = new StringBuilder("1");
        for(int i=0; i<N; i++) {
            sb.append("+" + i);
        }
    }

    @Test
    public void testStringBuilder2() {
        StringBuilder sb = new StringBuilder("1");
        for(int i=0; i<N; i++) {
            sb.append("+").append(i);
        }
    }

}
