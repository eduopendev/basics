package edu.opendev.basics;

import edu.opendev.basics.SwitchStatementDemo;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ralex on 26.08.16.
 */
public class WelcomeDemoTest extends Assert {

    @Test
    public void testSwitchBreakCase() {
        List<String> actualMonths = new ArrayList<>();
        for (int i = 1; i <= SwitchStatementDemo.months.size(); i++) {
            actualMonths.add(SwitchStatementDemo.breakCase(i, true));
        }

        assertArrayEquals(SwitchStatementDemo.months.toArray(), actualMonths.toArray());
        assertEquals(SwitchStatementDemo.INVALID_MONTH_NUMBER, SwitchStatementDemo.breakCase(0, true));
    }

    @Test
    public void testSwitchFallThrough() {
        int start = 10;
        String[] arrActualFutureMonths = SwitchStatementDemo.fallThrough(start, true).toArray(new String[0]);
        String[] arrAllMonths = (String[]) SwitchStatementDemo.months.toArray();
        String[] arrExpectedMonth = Arrays.copyOfRange(arrAllMonths, start - 1, arrAllMonths.length);

        assertArrayEquals(arrExpectedMonth, arrActualFutureMonths);

        String[] arrActualInvalidMonth = SwitchStatementDemo.fallThrough(0, true).toArray(new String[0]);
        String[] arrExpectedInvalidMonth = {SwitchStatementDemo.INVALID_MONTH_NUMBER};

        assertArrayEquals(arrExpectedInvalidMonth, arrActualInvalidMonth);
    }

    @Test
    public void testMultiCase() {

    }

    @Test
    public void testStringCase() {
        String[] arrAllMonths = (String[]) SwitchStatementDemo.months.toArray();
        for (int i = 0, mNum; i < arrAllMonths.length; i++) {
            mNum = SwitchStatementDemo.stringCase(arrAllMonths[i], true);
            assertEquals(i+1, mNum);
        }
    }

}
