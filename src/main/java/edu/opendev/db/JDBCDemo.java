package edu.opendev.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Map;
import java.util.Properties;

/**
 * Created by ralex on 27.09.16.
 */
public class JDBCDemo {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/EMP";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "1";

    public static void demoQuery() {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName(JDBC_DRIVER);
            String url = "jdbc:mysql://localhost:3306/test";
            connection = DriverManager.getConnection(url, USER, PASS);
            statement = connection.createStatement();
            //stmt.executeUpdate("delete FROM students WHERE id=1");
            resultSet = statement.executeQuery("select * from students");
            while (resultSet.next()) {
                //String str = rs.getString(1) + ":" + rs.getString(2);
                String str = resultSet.getString("firstName") + ":" + resultSet.getString("lastName");
                System.out.println(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.err.println("Error: " + ex.getMessage());
            }
        }

    }

    public static void test() {
        FileInputStream inputStream = null;
        try {
            //String pFN = "properties.tmp";
            String pFN = "gradle.properties";
            inputStream = new FileInputStream(new File(pFN));
            Properties properties = new Properties();
            properties.load(inputStream);
            String sonarUrl = properties.getProperty("systemProp.sonar.host.url");
            //sonarUrl = properties.getProperty("...", "default value");
            //properties.entrySet();

            System.out.println("-----");
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                System.out.println("ключ: " + entry.getKey());
                System.out.println("значение: " + entry.getValue());
                System.out.println("-----");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e.getMessage());// e.printStackTrace();
        } finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
