package edu.opendev.patterns.strategy;

/**
 * Created by ralex on 09.09.16.
 */
public class Context {

    private Strategy strategy;

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void doSomething() {
        strategy.execute();
    }

}
