package edu.opendev.patterns.strategy;

/**
 * Created by ralex on 09.09.16.
 */
public interface Strategy {
    void execute();
}
