package edu.opendev.patterns.decorator;

/**
 * Created by ralex on 27.10.16.
 */


class MainComponent implements InterfaceComponent {

    @Override
    public void doOperation() {
        System.out.print("World!");
    }
}
