package edu.opendev.patterns.decorator;

/**
 * Created by ralex on 27.10.16.
 */
class DecoratorSpace extends Decorator{

    public DecoratorSpace(InterfaceComponent c) {
        super(c);
    }

    @Override
    public void doOperation() {
        System.out.print(" ");
        super.doOperation();
    }

    @Override
    public void newOperation() {
        System.out.println("New space operation");
    }
}
