package edu.opendev.patterns.decorator.coffee;

/**
 * Created by ralex on 27.10.16.
 */
public class CoffeeDemo {

    public static void printInfo(Coffee c) {
        System.out.println("Cost: " + c.getCost() + "; Ingredients: " + c.getIngredients());
    }

    public static void main(String[] args) {
        Coffee c = new SimpleCoffee();
        printInfo(c);

        c = new WithMilk(c);
        printInfo(c);

        c = new WithSprinkles(c);
        printInfo(c);

        Coffee c2 = new WithMilk(new WithSprinkles(new SimpleCoffee()));
        printInfo(c2);
    }

}
