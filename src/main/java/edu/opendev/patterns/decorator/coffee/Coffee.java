package edu.opendev.patterns.decorator.coffee;

/**
 * Created by ralex on 27.10.16.
 */
// The interface Coffee defines the functionality of Coffee implemented by decorator
public interface Coffee {
    double getCost(); // Returns the cost of the coffee
    String getIngredients(); // Returns the ingredients of the coffee
}