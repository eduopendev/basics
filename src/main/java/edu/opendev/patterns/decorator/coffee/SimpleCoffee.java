package edu.opendev.patterns.decorator.coffee;

/**
 * Created by ralex on 27.10.16.
 */
// Extension of a simple coffee without any extra ingredients
public class SimpleCoffee implements Coffee {
    @Override
    public double getCost() {
        return 1;
    }

    @Override
    public String getIngredients() {
        return "Coffee";
    }
}
