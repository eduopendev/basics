package edu.opendev.patterns.decorator;

/**
 * Created by ralex on 27.10.16.
 */
public interface InterfaceComponent {
    void doOperation();
}