package edu.opendev.patterns.decorator;

public class DecoratorDemo {

    public static void main(String... s) {

        MainComponent mainComponent = new MainComponent();
        mainComponent.doOperation();

        System.out.println();

        InterfaceComponent mainComponent1 = new DecoratorHello(new DecoratorComma(new DecoratorSpace(mainComponent)));

        //Decorator c = new DecoratorHello(new DecoratorComma(new DecoratorSpace(new MainComponent())));
        //c.doOperation(); // Результат выполнения программы "Hello, World!"
        //c.newOperation(); // New hello operation
    }
}
