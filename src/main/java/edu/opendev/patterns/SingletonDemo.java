package edu.opendev.patterns;

/**
 * Created by ralex on 08.09.16.
 */
public class SingletonDemo {

    private static SingletonDemo ourInstance = new SingletonDemo();

    public static SingletonDemo getInstance() {
        return ourInstance;
    }

    private SingletonDemo() {

    }

    public void makeSomethig() {
        System.out.println("Привет, я " + this +  ", и я один объект на весь проект, больше таких как я нет!)");
    }

}
