package edu.opendev.exceptions;

/**
 * Created by ralex on 29.09.16.
 */
public class MyUncheckedExc extends RuntimeException {

    public MyUncheckedExc(String message, Throwable cause) {
        super(message, cause);
    }

}
