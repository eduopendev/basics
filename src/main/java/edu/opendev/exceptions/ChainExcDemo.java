package edu.opendev.exceptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static edu.opendev.Main.hr;

/**
 * Created by ralex on 28.09.16.
 */
public class ChainExcDemo {

    /**
     * Метод бросает исключение, указывая для него в качестве причины другое исключение через конструктор
     * @throws IllegalArgumentException
     */
    private static void throwChainExc1() throws IllegalArgumentException {
        throw new IllegalArgumentException("верхний уровень 1", new NullPointerException("причина 1"));
    }

    /**
     * Метод бросает исключение, указывая для него в качестве причины другое исключение через метод initCause, т.к.
     * конструтора с причиной для такого исключения нет
     */
    private static void throwChainExc2() {
        // создаем исключение, у которого отсутствует конструктор с указанием исключения-причины
        NullPointerException е = new NullPointerException("верхний уровень 2");
        // выставляем причину (это можно сделать только один раз)
        е.initCause(new ArithmeticException("причина 2"));
        //бросаем исключение
        throw е;
    }

    /**
     * Метод бросает пользовательское исключение, указывая для него в качестве причины
     * другое исключение через конструктор
     * @throws MyException
     */
    private static void throwChainExc3() throws MyException {
        throw new MyException("верхний уровень 3", new ArithmeticException("причина 3"));
    }

    public static void main(String args[]) {

        try {
            throwChainExc1();
        } catch (IllegalArgumentException e) {
            showExc(e);
        }


        try {
            throwChainExc2();
        }
        catch(NullPointerException e) {
            showExc(e);
        }

        try {
            throwChainExc3();
        } catch (MyException e) {
            showExc(e);
        } finally {
            System.out.println("finally блок отрабатывает в любом случае");
        }

        /**
         * исключение не возникает, хотя бросается и не ловится в этом методе, т.к. в блоке finally написан return
         */
        throwWrongExc();


        try {
            wrapExc();
        } catch (MyException e) {
            showExc(e);
        }

        /**
         * Обрабатывая несколько раз обернутое исключение, можно добраться до первопричины рекурсивно вызывая
         * метод getCause(), пока он не вернет null
         */
        try {
            wrapChainedExc();
        } catch (MyException e) {
            /*
            // отобразить исключение верхнего уровня
            System.out.println("Перехвачено: " + e) ;
            // отобразить исключение-причину
            System.out.println("Предыдущаю причина: " + e.getCause()) ;
            System.out.println("Первопричина: " + e.getCause().getCause()) ;
            hr();*/
            showAllChainedExc(e);
        }
        hr();

        try {
            /**
             * Метод может бросить непроверяемое исключение, в котором завернуто проверяемое
             */
            wrapCheckedExcInUnchecked();
        } catch (Exception e) {
            showAllChainedExc(e);
        }
    }

    /**
     * Пример некорректного блока try, если в finally поставить return, то исключение потеряется
     * @return
     */
    private static int throwWrongExc() {
        try {
            throw new MyException("верхний уровень 4", new ArithmeticException("причина 4"));
        } finally {
            System.out.println("в finally нельзя писать return");
            return 0;
        }
    }

    /**
     * Метод оборачивает стандартное исключение и бросает свое
     * @throws MyException
     */
    private static void wrapExc() throws MyException {

        try {
            FileInputStream fileInputStream = new FileInputStream("nn.n");
        } catch (FileNotFoundException e) {
            throw new MyException("свое сообщение", e);
        }

    }

    /**
     * Метод оборачивает chained исключение и бросает свое
     * @throws MyException
     */
    private static void wrapChainedExc() throws MyException {
        try {
            throwChainExc1();
        } catch (Exception e) {
            throw new MyException("свое сообщение 2", e);
        }
    }

    /**
     * Метод оборачивает проверяемое исключение в непроверяемое
     */
    private static void wrapCheckedExcInUnchecked() {

        try {
            throwChainExc3();
        } catch (MyException e) {
            throw new MyUncheckedExc("my comment", e);
        }

    }

    private static void showExc(Exception e) {
        // отобразить исключение верхнего уровня
        System.out.println("Перехвачено: " + e) ;
        // отобразить исключение-причину
        System.out.println("Исходная причина: " + e.getCause()) ;
        hr();
    }

    /**
     * рекурсивно проходит по причинам исключения и отображает их
     * @param e
     */
    private static void showAllChainedExc(Exception e) {
        // отобразить исключение верхнего уровня
        System.out.println("Перехвачено: " + e) ;
        Throwable cause = e.getCause();
        int n = 1;
        while (cause != null) {
            System.out.println("причина " + n + ": " + cause);
            cause = cause.getCause();
            n++;
        }
    }
}
