package edu.opendev.generics;

import edu.opendev.copy_objects.Author;
import edu.opendev.copy_objects.Book;
import edu.opendev.copy_objects.BookEx;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by ralex on 04.10.16.
 */
public class GenDemo {

    public static void main(String[] args) {

        /**
         * Для каждой обобщенной коллекции (и любого обобщенного класса) есть простой класс с тем же именем
         * для обратной совместимости со старыми версиями java
         */
        ArrayList arrayList = new ArrayList();
        /**
         * В не типизированную коллекцию можно добавлять произвольные объекты, но
         */
        arrayList.add(3);
        arrayList.add("str");
        arrayList.add(new BookEx("Астронавт Джонс", "Р.Хайнлайн"));
        /**
         * но при этом, нужно делать кастование типов и следить за его правильностью
         */
        Integer value = (Integer) arrayList.get(0);
        String strValue = (String) arrayList.get(1);
        Book book = (Book) arrayList.get(2);

        /**
         * примеры работы с обобщенным классами
         */
        oneGen();
        twoGen();
        rndList();

        BookEx bookEx = new BookEx("книга2", "Мы");
        Book book1 = new Book("книга", new Author("Я"));
        //book1 = bookEx;

        OneGen<BookEx> bookExOneGen = new OneGen<>(bookEx);
        OneGen<Book> bookOneGen = new OneGen<>(book1);
        //bookOneGen = bookExOneGen; - такое присваивание недопустимо, отношение наследования не сохраняется
        /**
         * ссылку на row-класс можно использовать для работы с обобщенными классами, но при сет-методах будут проблемы
         */
        OneGen oneGen = bookExOneGen;
        oneGen.print();
        oneGen = bookOneGen;
        oneGen.print();

        /**
         * Демонстрация работы с подстановочными типами
         */
        wildCard();

    }

    public static void oneGen() {
        System.out.println("простой обобщенный класс-контейнер объекта");

        OneGen<String> stringOneGen = new OneGen<>("тестовая строка");
        stringOneGen.print();
        String obj = stringOneGen.getObj();
        System.out.println(obj);

        //с простыми типами - не работает
        //GenDemo<int> genDemo;
        System.out.println();

        OneGen<Integer> integerOneGen = new OneGen<>(77);
        integerOneGen.print();
        Integer n = integerOneGen.getObj();
        System.out.println(n);

        System.out.println();

        OneGen<BookEx> bookExOneGen = new OneGen<>(new BookEx("Три мушкетера", new Author("А.Дюма")));
        bookExOneGen.print();
        BookEx bookEx = bookExOneGen.getObj();
        System.out.println(bookEx);

        System.out.println();

        ArrayList<String> list = new ArrayList<>();
        list.add("понеделник");
        list.add("вторник");
        list.add("среда");

        OneGen<ArrayList<String>> oneGenWithList = new OneGen<>(list);
        oneGenWithList.print();


        System.out.println();
    }

    public static void twoGen() {
        System.out.println("простой обобщенный класс-контейнер пары объектов");
        TwoGen<Integer, BookEx> exTwoGen = new TwoGen<>(33, new BookEx("Дверь в лето", new Author("Р.Хайнлайн")));
        exTwoGen.print();
        System.out.println();
    }

    public static void rndList() {
        RandomList<String> randomList = new RandomList<>();
        String base = "Некоторые могут задуматься, что беспорядок в коде увеличился, но это не так. Вместо приведения" +
                " к Integer в строчке 3, у нас теперь есть Integer в качестве параметра в строчке 1. Здесь" +
                " существенное отличие. Теперь компилятор может проверить этот тип на корректность во время компиляции.";
        for(String word : base.split(" ")) {
            randomList.add(word);
        }



        for (int i = 0; i< 15; i++) {
            System.out.print(randomList.select() + " ");
        }
        System.out.println();
    }

    public static void printObj(Object obj) {
        System.out.println("тип объекта: " + obj.getClass().getName());
        System.out.println("строковое представление: " + obj.toString());
    }

    /**
     * Пример обобщенного метода
     * @param a
     * @param <T>
     * @return
     */
    public static <T> T getMiddle(T... a) {
        return a[a.length / 2];
    }

    /**
     * демонстрация работы с подстановочными типами
     */
    public static void wildCard() {
        List<String> stringList = new ArrayList<>(
                Arrays.asList("понедельник", "вторник", "среда")
        );

        System.out.println(stringList.toString());
        dump(stringList);

        List<Integer> integerList = new ArrayList<>(
                Arrays.asList(23,32,55,66)
        );
        System.out.println(integerList);
        dump(integerList);

        System.out.println();

        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("book1", "author1"));
        bookList.add(new Book("book2", "author2"));

        List<BookEx> bookExList = new ArrayList<>();
        bookExList.add(new BookEx("bookEx1", "authorEx1"));
        bookExList.add(new BookEx("bookEx2", "authorEx2"));

        printBook(bookList);
        printBook(bookExList);

        swap(bookExList, 0, 1);
        printBook(bookExList);
    }

    static void printBook(List<? extends Book> c) {
        for (Book s : c) {
            s.print();
        }
    }

    /**
     * Метод использует подстановочный тип
     * @param c
     */
    static void dump(Collection<?> c) {
        for (Iterator<?> i = c.iterator(); i.hasNext(); ) {
            Object o = i.next();
            System.out.println(o);
        }
    }

    /**
     * Метод использует подстановочный тип, передавая его в обобщенный метод
     * @param list
     * @param i
     * @param j
     */
    static void swap(List<?> list, int i, int j) {
        swapImpl(list, i, j);
        //list.set(i, list.get(j));
    }

    static <T> void swapImpl(List<T> list, int i, int j) {
        T temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }

}
