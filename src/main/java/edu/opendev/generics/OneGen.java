package edu.opendev.generics;

/**
 * Created by ralex on 04.10.16.
 */
public class OneGen<T> {

    private final T obj;

    public OneGen(T obj) {
        this.obj = obj;
    }

    public T getObj() {
        return obj;
    }

    public void print() {
        GenDemo.printObj(obj);
    }
}
