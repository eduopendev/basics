package edu.opendev.generics;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ralex on 04.10.16.
 */
public class RandomList<T> {

    private ArrayList<T> storage = new ArrayList<>();
    private Random rnd = new Random();

    public void add(T item) {
        storage.add(item);
    }

    public T select() {
        return storage.get(rnd.nextInt(storage.size()));
    }

}
