package edu.opendev.classtypes;

import edu.opendev.basics.ControlFlowDemo;

import java.util.Arrays;
import java.util.List;

import static edu.opendev.Main.hr;

/**
 * Внешний класс
 * Created by ralex on 07.09.16.
 */
public class OuterClassDemo {

    private String outerPrivateField = "закрытое поле внешнего класса";
    public String outerPublicField = "открытое поле внешнего класса";
    protected String outerProtectedField = "защищенное поле внешнего класса";

    private static String outerPrivateStaticField = "закрытое статическое поле внешнего класса";
    public static String outerPublicStaticField = "открытое статическое поле внешнего класса";
    protected static String outerProtectedStaticField = "защищенное статическое поле внешнего класса";

    private void outerPrivateMethod() {
        System.out.println("закрытый метод внешнего класса");
    }

    public void outerPublicMethod() {
        System.out.println("открытый метод внешнего класса");
    }

    protected void outerProtectedMethod() {
        System.out.println("защищенный метод внешнего класа");
    }

    private static void outerPrivateStaticMethod() {
        System.out.println("закрытый статический метод внешнего класса");
    }

    public static void outerPublicStaticMethod() {
        System.out.println("открытый статический метод внешнего класса");
    }

    protected static void outerProtectedStaticMethod() {
        System.out.println("защищенный статический метод внешнего класа");
    }

    private String overlapField = "защищенное поле внешнего класса с перекрывающимся именем";
    private void overlapMethod() {
        System.out.println("защищенный метод внешнего класса с перекрывающимя именем");
    }

    private static String overlapStaticField = "защищенное статическое поле внешнего класса с перекрывающимся именем";
    private static void overlapStaticMethod() {
        System.out.println("защищенный статический метод внешнего класса с перекрывающимя именем");
    }

    /**
     * Вложенный класс
     * вложенные (nested) классы и интерфейсы — используются для задания совершенно самостоятельных классов и
     * интерфейсов внутри классов. Должны при задании иметь модификатор static. Имя класса верхнего уровня используется
     * в качестве квалификатора в пространстве имен, во всем остальном они ведут себя как обычные классы.
     * Кроме этого, static nested классы имеют доступ к любым статическим методам внешнего класса,
     * в том числе и к приватным.
     * Вложенные классы могут иметь как статические поля и методы, так и обычные
     */
    public static class StaticNestedClassDemo {

        private String nestedPrivateField = "приватное поле вложенного класса";
        private static String nestedPrivateStaticField = "приватное статическое поле вложенного класса";
        public static String nestedPublicStaticField = "открытое статическое поле вложенного класса";

        private static String overlapStaticField = "защищенное статическое поле вложенного класса " +
                "с перекрывающимся именем";
        private static void overlapStaticMethod() {
            System.out.println("защищенный статический метод вложенного класса с перекрывающимя именем");
        }

        /**
         * Перечисление, вроженное во вложенный класс, доступно снаружи через квалификаторы имен своих внешних классов
         */
        public enum EnumInNestedClass {NORTH, SOUTH, EAST, WEST}

        public StaticNestedClassDemo() {
            System.out.println("Конструктор StaticNestedClassDemo");
            System.out.println("Есть доступ к");
            System.out.println(outerPrivateStaticField);
            System.out.println(outerProtectedStaticField);
            System.out.println(outerPublicStaticField);
            outerPrivateStaticMethod();
            outerProtectedStaticMethod();
            outerPublicStaticMethod();

            /**
             * если имя статического поля или метода такое же, как во внешнем классе, то просто по имени мы получим
             * поле или метод вложенного класса, т.е. имена перекрываются
             */
            System.out.println(overlapStaticField);
            overlapStaticMethod();
            /**
             * чтобы получить по такому же имени поле или метод внешнего класса, нужно указать квалификатор имени класса
             */
            System.out.println(OuterClassDemo.overlapStaticField);
            OuterClassDemo.overlapStaticMethod();
        }

        public String getNestedPrivateField() {
            return nestedPrivateField;
        }

        public static String getNestedPrivateStaticField() {
            return nestedPrivateStaticField;
        }

        public static String getOuterPrivateStatic() {
            return outerPrivateStaticField;
        }
    }

    /**
     * Вложенное перечисление
     */
    public enum NestedEnum {LEFT, RIGHT, UP, DOWN}

    /**
     * Внутренний класс (member inner class, или non-static nested class)
     * Ассоциируются с объектом внешнего класса, имеют доступ ко всем полям и методам внешненго класса
     */
    public class InnerClassDemo {

        /**
         * Внутренний класс не может иметь статических полей и методов. Если убрать комментарии с двух объявлений ниже,
         * то получим ошибку.
         */
        //private static String wrongFieldDeclaration;
        //private static void wrongMethodDeclaration() { }

        /**
         * кроме того, внутренний класс не может иметь вложенных перечислений
          */
        //public enum EnumInInnerClass {ON, OFF}

        private String innerPrivateField = "закрытое поле внутреннего класса";
        public String innerPublicField = "открытое поле внутреннего класса";
        protected String innerProtectedField = "защищенное поле внутреннего класса";

        private String overlapField = "защищенное поле внутреннего класса с перекрывающимся именем";
        private void overlapMethod() {
            System.out.println("защищенный метод внутреннего класса с перекрывающимя именем");
        }

        /**
         * Открытый конструктор
         * Объект внутреннего класса можно инстанцировать только через объект внешнего класса
         */
        public InnerClassDemo() {

            System.out.println("Конструктор InnerClassDemo");
            System.out.println("Есть доступ к");
            //статическим полям и методам внешнего класса
            System.out.println(outerPrivateStaticField);
            System.out.println(outerProtectedStaticField);
            System.out.println(outerPublicStaticField);
            outerPrivateStaticMethod();
            outerProtectedStaticMethod();
            outerPublicStaticMethod();
            //не статическим полям и методам внешнего класса
            System.out.println(outerPrivateField);
            System.out.println(outerProtectedField);
            System.out.println(outerPublicField);
            outerPrivateMethod();
            outerProtectedMethod();
            outerPublicMethod();
            //к вложенным перечислениями, влючая вложенные во вложенные статические классы
            System.out.println(Arrays.toString(NestedEnum.values()));
            System.out.println(Arrays.toString(StaticNestedClassDemo.EnumInNestedClass.values()));
            //к статическим полям и методам вложенного класса
            System.out.println(StaticNestedClassDemo.nestedPrivateStaticField);
            System.out.println(StaticNestedClassDemo.nestedPrivateStaticField);
            StaticNestedClassDemo.getNestedPrivateStaticField();

            /**
             * если имя поля или метода такое же, как во внешнем классе, то по этому имени мы получим поле или метод
             * вложенного класса, т.е. имена перекрываются
             */
            System.out.println(overlapField);
            overlapMethod();

            /**
             * добраться до такого же имени во внешнем классе можно, указав квалификатор имени внешнего класса и
             * ключевое слово this. В этом случае this будет указывать на инстанс внешнего класса, с помощью которого
             * был создан инстанс внутренего класса
             */
            System.out.println(OuterClassDemo.this.overlapField);
            OuterClassDemo.this.overlapMethod();
            System.out.println(this.overlapField);
        }


    }

    public void workInside() {
        /**
         * Создание экземпляра вложенного класса не требует указания имени внешнего класса, т.к. мы уже в нем
         */
        StaticNestedClassDemo instanceOfStaticNestedClassDemo = new StaticNestedClassDemo();
        /**
         * Создание экземпляра внутреннего класса не требует указания объекта внешнего класса, но при желаии его можно
         * и указать - т.е. написать this
         */
        InnerClassDemo instanceOfInnerClassDemo = this.new InnerClassDemo();
        InnerClassDemo instanceOfInnerClassDemo2 = new InnerClassDemo();
    }

    public static void staticWork(OuterClassDemo outerClassDemo) {
        InnerClassDemo obj = outerClassDemo.new InnerClassDemo();
    }

    /**
     * можно объявить внутренний интерфейс, и имплементировать его во внутренних и вложенных классах
     * если интерфейс объявить как public, то его можно будет реализовать и другим внешним классам
     */
    public interface Connector {
        void connect();
    }

    /**
     * Также можно объявлять абстрактные классы
     * Абстрактный вложенный (статический) класс могут наследовать как вложенные, так и внутренние классы
     * А если он объявлен как public, то и внешние классы
     */
    abstract public static class AbstractStaticNestedClass {
        void makeSomething() {
            System.out.println("make");
        }
        abstract public void doSomething();
        public static void staticMethod() {
            System.out.println("static");
        }
    }

    public class IC extends AbstractStaticNestedClass implements Connector {

        @Override
        public void connect() {

        }

        @Override
        public void doSomething() {
            makeSomething();
            staticMethod();
        }
    }

    public static class NC extends AbstractStaticNestedClass implements Connector {

        @Override
        public void connect() {

        }

        @Override
        public void doSomething() {

        }
    }

    /**
     * Абстрактный внутренний класс (не статик) могут наследовать только внутренние классы, вложенные не могут
     */
    abstract public class AbstractInnerClass {
        public void makeSomething() {
            System.out.println("make");
        }
        public abstract void doSomething();
    }

    public class IC2 extends AbstractInnerClass implements Connector {

        @Override
        public void connect() {

        }

        @Override
        public void doSomething() {

        }
    }

    /**
     * Такое наследование ошибочно -
     */
    //public static class NC2 extends AbstractInnerClass  {    }


    public void sampleLocalClass(final int temp, int simple) {

        /**
         * внутри любого блока или метода можно объявить локальный класс.
         * Как и member классы, локальные классы ассоциируются с экземпляром обрамляющего класса и имеют доступ
         * к его полям и методам. Кроме этого, локальный класс может обращаться к локальным переменным
         * и параметрам метода, если они объявлены с модификатором final.
         * У локальных классов есть множество ограничений:
         * они видны только в пределах блока, в котором объявлены;
         * они не могут быть объявлены как private, public, protected или static;
         * они не могут иметь внутри себя статических объявлений (полей, методов, классов),
         *      исключением являются константы (static final);
         * интерфейсы нельзя объявлять локально
         */

        if(temp > 5) {

            class TempWorker {
                void make() {
                    //outerPrivateField;
                }
            }

            TempWorker tm = new TempWorker();
            tm.make();


        }

        class LocalClass implements Connector {

            static final int n = 10;
            int a;
            //ошибка -
            //static int b;

            @Override
            public void connect() {
                //видно все поля и методы внешнего класса
                String locStr = outerPrivateField;
                outerPrivateMethod();
                outerPrivateStaticMethod();
                //так - можно
                int z = temp;
                z = simple;
                //так - нельзя
                //simple = 6;
                //simple = temp;

            }
        }

        LocalClass lc = new LocalClass();
        lc.connect();

    }

    Runnable obj = new Runnable() {
        @Override
        public void run() {
            System.out.println("anon class 1");
        }
    };

    Runnable obj2 = new Runnable() {
        @Override
        public void run() {
            System.out.println("anon class 2");
        }
    };

    public static void main(String[] args) {
        /**
         * Вложенный статик-класс можно использовать как обычный класс, но в имени добавляется квалификатор внешнего
         * класса. При этом, вложенный класс имеет доступ ко всем статическим полям и методам своего внешнего класса,
         * включая приватные. Здесь создается объект вложенного класса, его конструктор выведет в консоль то что он
         * видит во внешнем классе.
         */
        OuterClassDemo.StaticNestedClassDemo instanceOfStaticNestedClass = new OuterClassDemo.StaticNestedClassDemo();
        hr();
        /**
         * созданный объект вложенного класса - обычный объект, можно пользоватья его открытыми методами,
         * получим через отрытый метод значение приватого поля -
         */
        System.out.println("работаем с объектом вложенного класса");
        String strValue = instanceOfStaticNestedClass.getNestedPrivateField();
        System.out.println(strValue);
        hr();
        /**
         * Открытые статические методы и поля вложенного класса доступны как обычно,
         * но с квалификатором имени внешнего класса
         */
        System.out.println("работаем со статическими методами и свойствами вложенного класса");
        strValue = OuterClassDemo.StaticNestedClassDemo.getNestedPrivateStaticField();
        System.out.println(strValue);
        strValue = OuterClassDemo.StaticNestedClassDemo.nestedPublicStaticField;
        System.out.println(strValue);
        hr();

        /**
         * То же и с вложенными перечислениями - доступны через квалификаторы
         */
        System.out.println("работаем с вложенным перечислением");
        strValue = Arrays.toString( OuterClassDemo.NestedEnum.values() );
        System.out.println(strValue);
        System.out.println("работаем с перечислением, вложенным во вложенный класс");
        strValue = Arrays.toString(OuterClassDemo.StaticNestedClassDemo.EnumInNestedClass.values());
        System.out.println(strValue);
        hr();

        /**
         * Внутренний класс тоже указывается с квалификатором имени внешнего класса, но сделать инстанс для него
         * можно только имея объект внешнего класса
         */
        // такое инстанцирование вызовет ошибку -
        //OuterClassDemo.InnerClassDemo instanceOfInnerClassDemo = new OuterClassDemo.InnerClassDemo();
        // нужно так -
        OuterClassDemo instanceOfOuterClass = new OuterClassDemo();
        OuterClassDemo.InnerClassDemo instanceOfInnerClassDemo = instanceOfOuterClass.new InnerClassDemo();

        /**
         * делаем инстанс внутреннего класса, который отнаследован от внутреннего класса другого класса)
         */
        ChildOfOuterClass o = new ChildOfOuterClass();
        ChildOfOuterClass.II ii = o.new II();
        hr();
    }



}
