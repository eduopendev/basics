package edu.opendev.classtypes;

/**
 * Класс, наследованный от класса, в котором объявлены вложенные и внутрениие классы, перечисления и интерфейсы.
 * Интерфейс, вложенный в классе-родителе, можно реализовать
 */
public class ChildOfOuterClass extends OuterClassDemo implements OuterClassDemo.Connector {

    public ChildOfOuterClass() {
        NestedEnum.values();
        StaticNestedClassDemo.EnumInNestedClass.values();

    }

    @Override
    public void connect() {

    }

    /**
     * Вложенный класс, наследник вложенного класса из другого класса
     */
    public static class Nested extends StaticNestedClassDemo {

    }

    /**
     * Внутренний класс, наследник вложенного класса из другого класса
     * В общем, открытые вложенные классы можно наследовать где угодно
     */
    public class Inner extends StaticNestedClassDemo {

    }

    /**
     * Поскольку мы являемсы наследником класса, в котором есть внутренний класс, то
     * мы может отнаследовать для своего внутреннго класса, внутренний класс родителя
     */

    public class II extends InnerClassDemo {

    }
}


class FreeClass {
    /**
     * нельзя отнаследовать внутренний класс другого класса, не будучи для него наследником
     */
    //public class Inner extends OuterClassDemo.InnerClassDemo {   }
}


/**
 * Класс-наследник от класса, вложенного в другой класс
 */
class ImplANC extends OuterClassDemo.AbstractStaticNestedClass {

    @Override
    public void doSomething() {

    }

}

/**
 * Класс-наследник от класса, вложенного в другой класс
 */
class ImplNC extends OuterClassDemo.NC {

    @Override
    public void doSomething() {

    }
}

/**
 * Наследовать внутренний класс извне нельзя
 */
//class ImplIC extends OuterClassDemo.InnerClassDemo {}
