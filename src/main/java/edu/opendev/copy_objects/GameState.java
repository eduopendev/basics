package edu.opendev.copy_objects;

import java.util.Objects;

/**
 * Created by ralex on 08.09.16.
 */
public class GameState implements Cloneable {

    private int max;
    private int value;
    private int count;

    public GameState(int max, int value, int count) {
        this.max = max;
        this.value = value;
        this.count = count;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMax() {
        return max;
    }

    public int getValue() {
        return value;
    }

    public int getCount() {
        Objects ob;
        return count;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public GameState copy(GameState gameState) {
        return new GameState(gameState.max, gameState.value, gameState.count);
    }
}
