package edu.opendev.copy_objects;

/**
 * Created by ralex on 20.09.16.
 */
public class SerialObjWithStatic extends SerialObjDemo {

    private static String strStatic = "static value";
    private final static String strFinalStatic = "значение конечного статического поля";


    public SerialObjWithStatic(String title) {
        super(title);
    }

    public static String getStrStatic() {
        return strStatic;
    }

    public static void setStrStatic(String strStatic) {
        SerialObjWithStatic.strStatic = strStatic;
    }

    @Override
    public String toString() {
        return new StringBuilder(super.toString()).append(", strStatic: ").append(strStatic).toString();
    }
}
