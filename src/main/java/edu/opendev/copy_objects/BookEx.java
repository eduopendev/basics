package edu.opendev.copy_objects;

import java.util.Objects;

/**
 * пример класса с пепеопределенными методами toString, hashCode и equals
 */
public class BookEx extends Book {

    public BookEx(String title, Author author) {
        super(title, author);
    }

    public BookEx(Book book) {
        super(book.getTitle(), book.getAuthor());
    }

    public BookEx(String title, String authorName) {
        super(title, new Author(authorName));
    }

    @Override
    public String toString() {
        return new StringBuilder(getAuthor().getName()).append(". ").append(getTitle()).toString();
    }

    @Override
    public int hashCode() {
        //return super.hashCode();
        return 3*getTitle().hashCode() + 17*getAuthor().getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        if(obj instanceof BookEx) {
            Book book = (Book) obj;
            //строки сравниваем через equals, а не через ==
            /*isEqual = this.getTitle().equals(book.getTitle())
                    && this.getAuthor().getName().equals(book.getAuthor().getName());*/
            // есть статик метод для бозопасного сравнения - чтобы не нарваться на нулл значения ссылок
            isEqual = Objects.equals(getTitle(), book.getTitle())
                    && Objects.equals(getAuthor().getName(), book.getAuthor().getName());
        }
        return isEqual;
    }

    @Override
    public void print() {
        //super.print();
        System.out.println("BookEx: " + toString());
    }

    private void testPrivateMethod(String str, int n) {
        System.out.println(str + n);
    }

}
