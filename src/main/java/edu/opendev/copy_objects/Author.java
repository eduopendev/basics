package edu.opendev.copy_objects;

import java.io.Serializable;

/**
 * Created by ralex on 08.09.16.
 */
public class Author implements Serializable {

    private String name;
    private String token;

    private static Author cached;

    public Author(Author author) {
        name = author.name;
        token = author.token;
        cached = author;
    }

    public Author(String name, String token) {
        this.name = name;
        this.token = token;
    }

    public Author(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Author getInstance() {
        return new Author("NoName");
    }

    public static Author getInstance(Author author) {

        /*Author instance;
        if(cached == null) {
            instance = new Author(author);
            cached = instance;
        } else {
            instance = cached;
        }

        return instance;*/
        return new Author(author);
    }

}
