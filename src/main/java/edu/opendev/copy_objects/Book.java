package edu.opendev.copy_objects;

import java.io.Serializable;

/**
 * Created by ralex on 08.09.16.
 */
public class Book implements Serializable {

    private String title;
    private Author author;

    public Book(String title, Author author) {
        this.title = title;
        this.author = Author.getInstance(author);
    }

    public Book(String title, String authorName) {
        this(title, new Author(authorName));
    }

    public String getTitle() {
        return title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void print() {
        System.out.println(new StringBuilder("Book: ").append(getAuthor().getName()).
                append(". ").append(getTitle()).toString());
    }

}
