package edu.opendev.copy_objects;

import static edu.opendev.Main.hr;

/**
 * Created by ralex on 08.09.16.
 */
public class CopyObjDemo {

    public static void trouble() {

        Author author1 = new Author("Дэвид Дойч");
        Author author2 = Author.getInstance();

        Book book1 = new Book("Структура реальности", author1);
        Book book2 = new Book("О некоммерческих организациях в России", author2);


        System.out.println(book1.getAuthor().getName());
        author1.setName("Семен Горбунков");
        System.out.println(book1.getAuthor().getName());

        book2.setAuthor(author1);

    }

    public static void trouble2() {
        GameState state = new GameState(100, 50, 7);

        GameResult result1 = new GameResult("a", state);

        System.out.println(
                new StringBuilder("Result 1: count = ")
                        .append(result1.getCount())
                        .append(" value = ")
                        .append(result1.getValue()).toString()
        );

        StringBuilder sb = new StringBuilder();

        state.setValue(25);
        state.setCount(15);

        GameResult result2 = new GameResult("b", state);
        System.out.println("Result 2: count = " + result2.getCount() + " value = " + result2.getValue());
        System.out.println("Result 1: count = " + result1.getCount() + " value = " + result1.getValue());


    }

    public static void main(String[] args) {
        /**
         * примеры с копированием объектов
         */
        CopyObjDemo.trouble();
        hr();
        CopyObjDemo.trouble2();
        hr();
    }

}
