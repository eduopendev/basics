package edu.opendev.copy_objects;

import java.io.*;
import java.util.*;

import static edu.opendev.Main.hr;

/**
 * Created by ralex on 20.09.16.
 */
public class SerialObjDemo implements Serializable {

    private String title;
    private int data[];
    private transient String transientField = "не сериализуемое поле";
    private InnerClass innerClassInst;

    private class InnerClass implements Serializable {
        private String innerField = "field of inner class";
    }

    public SerialObjDemo() {
        this("none");
    }

    public SerialObjDemo(String title) {
        this.title = title;
        this.data = new int[10];
        Random random = new Random();
        for (int i=0; i<data.length; i++) {
            data[i] = random.nextInt(100) + 1;
        }
        innerClassInst = new InnerClass();
    }

    public String getTitle() {
        return title;
    }

    public int[] getData() {
        return data;
    }

    public String getTransientField() {
        return transientField;
    }

    @Override
    public String toString() {
        return new StringBuilder("title: ").append(title).append(", data: ")
                .append(Arrays.toString(data)).append(", transientField: ").append(transientField).toString();
    }

    public static void main(String[] args) {
        /**
         * пример с сериализацией
         */
        System.out.println("пример с сериализацией");
        SerialObjDemo serialObj = new SerialObjDemo("test");
        System.out.println(serialObj);

        try {
            //сериализуем и запишем в файл единичный объект
            String fileName = "serialObj.tmp";
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(serialObj);

            //считаем его из файла, видим что transient поле не десериализовано
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            SerialObjDemo serialObj2 = (SerialObjDemo) objectInputStream.readObject();
            System.out.println(serialObj2);

            //в тот же файл запишем объект со статичным полем
            SerialObjWithStatic serialObjWithStatic = new SerialObjWithStatic("тест статик");
            System.out.println(serialObjWithStatic);
            objectOutputStream.writeObject(serialObjWithStatic);
            //теперь поменяет его значение
            SerialObjWithStatic.setStrStatic("new static value");
            //десериализуем объект
            SerialObjWithStatic serialObjWithStatic2 = (SerialObjWithStatic) objectInputStream.readObject();
            //видим, что после десериализации статик поле не перезаписалось, для него исп.значение из программы
            System.out.println(serialObjWithStatic2);

            objectOutputStream.close();
            fileOutputStream.close();
            objectInputStream.close();
            fileInputStream.close();

            //сделаем новый файл для записи сериализованных объектов
            String fileName2 =  "mix.tmp";
            fileOutputStream = new FileOutputStream(fileName2);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            String str = "строка";
            Date date = new Date();
            serialObj = new SerialObjDemo();
            List<Object> list = new ArrayList<>();
            list.add(str);
            list.add(date);
            list.add(serialObj);
            Book bookEx = new BookEx("Три мушкетера", new Author("А. Дюма"));
            //запишем в поток различные объекты, включая список объектов
            objectOutputStream.writeObject(str);
            objectOutputStream.writeObject(date);
            objectOutputStream.writeObject(serialObj);
            objectOutputStream.writeObject(list);
            objectOutputStream.writeObject(bookEx);
            objectOutputStream.close();
            fileOutputStream.close();

            //теперь прочитаем объекты из файла, читать нужно в той же последовательности, не в обратной
            fileInputStream = new FileInputStream(fileName2);
            objectInputStream = new ObjectInputStream(fileInputStream);
            str = (String) objectInputStream.readObject();
            date = (Date) objectInputStream.readObject();
            serialObj = (SerialObjDemo) objectInputStream.readObject();
            list = (List<Object>) objectInputStream.readObject();
            bookEx = (Book) objectInputStream.readObject();
            System.out.println(str);
            System.out.println(date);
            System.out.println(serialObj);
            System.out.println(list);
            System.out.println(bookEx);

            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        hr();
    }
}
