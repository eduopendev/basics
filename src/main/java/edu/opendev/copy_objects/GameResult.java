package edu.opendev.copy_objects;

/**
 * Created by ralex on 08.09.16.
 */
public class GameResult {

    private String name;
    GameState state;

    public GameResult(String name, GameState state) {
        this.name = name;

        try {
            this.state = (GameState) state.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return state.getCount();
    }

    public int getValue() {
        return state.getValue();
    }




}
