package edu.opendev.basics;

import static edu.opendev.Main.hr;

/**
 * Created by ralex on 30.08.16.
 */
public class ArrayDemo {

    public static void dimArray() {

        System.out.println("Примеры одномерных массивов");

        // declares an array of integers
        int[] anArray;

        // allocates memory for 10 integers
        anArray = new int[10];

        // initialize first element
        anArray[0] = 100;
        // initialize second element
        anArray[1] = 200;
        // and so forth
        anArray[2] = 300;
        anArray[3] = 400;
        anArray[4] = 500;
        anArray[5] = 600;
        anArray[6] = 700;
        anArray[7] = 800;
        anArray[8] = 900;
        anArray[9] = 1000;

        //Alternatively, you can use the shortcut syntax to create and initialize an array:

        int[] anArray2 = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000};

        System.out.println("Element at index 0: " + anArray[0]);
        System.out.println("Element at index 1: " + anArray[1]);
        System.out.println("Element at index 2: " + anArray[2]);
        System.out.println("Element at index 3: " + anArray[3]);
        System.out.println("Element at index 4: " + anArray[4]);
        System.out.println("Element at index 5: " + anArray[5]);
        System.out.println("Element at index 6: " + anArray[6]);
        System.out.println("Element at index 7: " + anArray[7]);
        System.out.println("Element at index 8: " + anArray[8]);
        System.out.println("Element at index 9: " + anArray[9]);

        //You can use the built-in length property to determine the size of any array. The following code prints the
        // array's size to standard output:

        System.out.println(anArray.length);

        //Similarly, you can declare arrays of other types:

        byte[] anArrayOfBytes;
        short[] anArrayOfShorts;
        long[] anArrayOfLongs;
        float[] anArrayOfFloats;
        double[] anArrayOfDoubles;
        boolean[] anArrayOfBooleans;
        char[] anArrayOfChars;
        String[] anArrayOfStrings;

        //You can also place the brackets after the array's name:
        // this form is discouraged
        float anArrayOfFloats2[];
        //However, convention discourages this form; the brackets identify the array type and should appear with the
        // type designation.



    }

    public static void multiDimArray() {

        System.out.println("Пример 'рваного' двумерного массива");

        String[][] names = {
                {"Mr. ", "Mrs. ", "Ms. "},
                {"Smith", "Jones"}
        };
        // Mr. Smith
        System.out.println(names[0][0] + names[1][0]);
        // Ms. Jones
        System.out.println(names[0][2] + names[1][1]);
        //выведет 2, а не 5, т.к. двумерный массив - это масссив массивов
        System.out.println(names.length);
    }

    public static void triangularMatrix() {

        System.out.println("Пример треугольной матрицы");

        int rowCount = 10;
        //объявление
        int[][] tm = new int[rowCount][];
        //инициализация
        for (int i = 0; i < tm.length; i++) {
            tm[i] = new int[i+1];
            for (int j = 0; j < i+1; j++) {
                tm[i][j] = j+1;
            }
        }
        //вывод в консоль
        for(int[] row : tm) {
            for (int value : row) {
                System.out.printf("%d", value);
            }
            System.out.println();
        }
    }

    public static void copyArrayDemo() {

        System.out.println("Примеры копирования массивов");

        char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a', 't', 'e', 'd' };
        char[] copyTo = new char[7];
        //Сигнатура метода System.arraycopy
        //void arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
        System.arraycopy(copyFrom, 2, copyTo, 0, 7);
        System.out.println(new String(copyTo));

        //Метод Arrays.copyOfRange перегружен, пример его сигнатуры для массивов типа char
        //char[] copyOfRange(char[] original, int from, int to)
        char[] copyTo2 = java.util.Arrays.copyOfRange(copyFrom, 2, 9);

        System.out.println(new String(copyTo2));

    }

    public static void main(String[] args) {
        /**
         * Примеры работы с массивами
         */
        ArrayDemo.dimArray();
        hr();
        ArrayDemo.multiDimArray();
        hr();
        ArrayDemo.triangularMatrix();
        hr();
        ArrayDemo.copyArrayDemo();
        hr();
    }

}
