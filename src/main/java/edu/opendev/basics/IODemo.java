package edu.opendev.basics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by ralex on 30.08.16.
 */
public class IODemo {


    /**
     * Пример простого файлового ввода-вывода
     * @throws FileNotFoundException
     */
    public static void simpleIO() throws FileNotFoundException {
        String dir = System.getProperty("user.dir");
        System.out.println(dir);

        PrintWriter printWriter = new PrintWriter("test.tmp");
        printWriter.println("Строка 1");
        printWriter.println("Строка 2");
        printWriter.close();

        Scanner in = new Scanner(new File("test.txt"));
        String str = in.nextLine();
        System.out.println(str);
        in.close();

        in = new Scanner(new File(dir + "/src/edu/opendev/Main.java"));
        while (in.hasNext()) {
            str = in.nextLine();
            System.out.println(str);
        }
        in.close();
    }

    public static void main(String[] args) {
        try {
            simpleIO();
        } catch (FileNotFoundException e) {
            System.out.println("файл не найден");
        }
    }
}
