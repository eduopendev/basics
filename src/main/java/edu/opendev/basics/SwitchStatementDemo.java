package edu.opendev.basics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static edu.opendev.Main.hr;

/**
 * Демонстрация работы оператора switch
 */
public class SwitchStatementDemo {

    protected static final List<String> months = Arrays.asList("Январь", "Февраль", "Март", "Апрель", "Май",
            "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");
    public static final String INVALID_MONTH_NUMBER = "Неправильный номер месяца";

    /**
     * Пример множественного выбора,
     * метод по номеру месяца возвращает и выводит в консоль его текстовое представление
     *
     * @param month номер месяца
     * @param quiet флаг для отключения вывода в консоль
     * @return текстовое представление месяца
     */
    public static String breakCase(int month, boolean quiet) {

        String monthString;
        switch (month) {
            case 1:
                monthString = months.get(0);
                break;
            case 2:
                monthString = months.get(1);
                break;
            case 3:
                monthString = months.get(2);
                break;
            case 4:
                monthString = months.get(3);
                break;
            case 5:
                monthString = months.get(4);
                break;
            case 6:
                monthString = months.get(5);
                break;
            case 7:
                monthString = months.get(6);
                break;
            case 8:
                monthString = months.get(7);
                break;
            case 9:
                monthString = months.get(8);
                break;
            case 10:
                monthString = months.get(9);
                break;
            case 11:
                monthString = months.get(10);
                break;
            case 12:
                monthString = months.get(11);
                break;
            default:
                monthString = INVALID_MONTH_NUMBER;
                break;
        }

        if (!quiet) {
            System.out.print("Я получил номер месяца = " + month + " и этот месяц - ");
            System.out.println(monthString);
        }

        return monthString;
    }

    /**
     *
     * @param month
     * @return
     */
    public static String breakCase(int month) {
        return breakCase(month, false);
    }

    /**
     * Пример использования переключателся без операторов break
     * Метод выводит и возвращает оставшиеся месяцы до конца года, включая указанный
     *
     * @param month номер месяца
     * @param quiet флаг для отключения вывода в консоль
     * @return список оставшихся месяцев в году
     */
    public static List<String> fallThrough(int month, boolean quiet) {
        List<String> futureMonths = new ArrayList<>();

        switch (month) {
            case 1:
                futureMonths.add(months.get(0));
            case 2:
                futureMonths.add(months.get(1));
            case 3:
                futureMonths.add(months.get(2));
            case 4:
                futureMonths.add(months.get(3));
            case 5:
                futureMonths.add(months.get(4));
            case 6:
                futureMonths.add(months.get(5));
            case 7:
                futureMonths.add(months.get(6));
            case 8:
                futureMonths.add(months.get(7));
            case 9:
                futureMonths.add(months.get(8));
            case 10:
                futureMonths.add(months.get(9));
            case 11:
                futureMonths.add(months.get(10));
            case 12:
                futureMonths.add(months.get(11));
        }

        if (futureMonths.isEmpty()) {
            futureMonths.add(INVALID_MONTH_NUMBER);
            //System.out.println("Неправильный номер месяца");
        }

        if(!quiet) {
            System.out.println("Вы указали номер месяца = " + month + ", оставшиеся месяцы в году");
            for (int i = 0; i < futureMonths.size(); i++) {
                System.out.println(futureMonths.get(i));
            }
        }


        return futureMonths;
    }

    /**
     *
     * @param month
     * @return
     */
    public static List<String> fallThrough(int month) {
        return fallThrough(month, false);
    }

    /**
     * Пример переключателя с множественными метками case
     * Выводит и возвращает число дней в месяце
     *
     * @param month номер месяца
     * @param year  год
     * @param quiet флаг для отключения вывода в консоль
     * @return число дней в месяце
     */
    public static int multiCase(int month, int year, boolean quiet) {
        int numDays = 0;

        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                numDays = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numDays = 30;
                break;
            case 2:
                if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
                    numDays = 29;
                } else {
                    numDays = 28;
                }
                break;
            default:
                if (!quiet) {
                    System.out.println(INVALID_MONTH_NUMBER);
                }
                break;
        }
        if (!quiet) {
            System.out.println("Количество дней в месяце с номером " + month + " и в " + year +  " году = " + numDays);
        }

        return numDays;
    }

    /**
     *
     * @param month
     * @param year
     * @return
     */
    public static int multiCase(int month, int year) {
        return multiCase(month, year, false);
    }

    /**
     * Пример переключателя по строковому значению
     * Выводит возвращает номер месяца по его текстовому представлению
     * @param month месяц (строкой по русски)
     * @param quiet флаг для отключения вывода в консоль
     * @return номер месяца
     */
    public static int stringCase(String month, boolean quiet) {

        int monthNumber;

        switch (month.toLowerCase()) {
            case "январь":
                monthNumber = 1;
                break;
            case "февраль":
                monthNumber = 2;
                break;
            case "март":
                monthNumber = 3;
                break;
            case "апрель":
                monthNumber = 4;
                break;
            case "май":
                monthNumber = 5;
                break;
            case "июнь":
                monthNumber = 6;
                break;
            case "июль":
                monthNumber = 7;
                break;
            case "август":
                monthNumber = 8;
                break;
            case "сентябрь":
                monthNumber = 9;
                break;
            case "октябрь":
                monthNumber = 10;
                break;
            case "ноябрь":
                monthNumber = 11;
                break;
            case "декабрь":
                monthNumber = 12;
                break;
            default:
                monthNumber = 0;
                break;
        }

        String message = monthNumber == 0 ? INVALID_MONTH_NUMBER : String.valueOf(monthNumber);
        if(!quiet) {
            System.out.println(message);
        }

        return monthNumber;
    }

    /**
     *
     * @param month
     * @return
     */
    public static int stringCase(String month) {
        return stringCase(month, false);
    }

    public static void main(String[] args) {
        SwitchStatementDemo.breakCase(1);
        hr();
        SwitchStatementDemo.fallThrough(10);
        hr();
        SwitchStatementDemo.stringCase("сЕнтЯБрь");
        hr();
        SwitchStatementDemo.multiCase(5, 2016);
        hr();
    }

}
