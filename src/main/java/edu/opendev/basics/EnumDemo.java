package edu.opendev.basics;

import java.util.Arrays;

import static edu.opendev.Main.hr;

/**
 *
 */
public class EnumDemo {

    /**
     * Перечисления в java - это особый класс, наследник от java.lang.Enum, множество допустимых значений которого огра-
     * ничено и задается в явном виде. В этом примере, возможны всего четыре объекта данного класса. Т.е. переменная
     * типа Direction dir может принимать всего четыре значения - LEFT, RIGHT, UP, DOWN, это - объекты данного класса,
     * доступные статически. Т.е. можно присвоить например dir = Direction.LEFT; и т.п.     *
     */
    public enum Direction {
        LEFT, RIGHT, UP, DOWN
    }


    interface Nameable {
        String getName();
    }

    /**
     * Перечисления нельзя наследовать, но они могут реализовавыть интерфейсы
     * Также они могут иметь поля и методы. Могут иметь конструкторы для объектов.
     */
    public enum DirectionEx implements Nameable {

        LEFT ("L") {
            @Override
            public DirectionEx inverse() {
                return RIGHT;
            }

            @Override
            public String getName() {
                return "налево";
            }
        },
        RIGHT ("R") {
            @Override
            public DirectionEx inverse() {
                return LEFT;
            }

            @Override
            public String getName() {
                return "направо";
            }
        },
        UP ("U") {
            @Override
            public DirectionEx inverse() {
                return DOWN;
            }

            @Override
            public String getName() {
                return "вверх";
            }
        },
        DOWN ("D") {
            @Override
            public DirectionEx inverse() {
                return UP;
            }

            @Override
            public String getName() {
                return "вниз";
            }
        };

        /**
         * приватное поле в перечислении
         */
        private String abbr;

        /**
         * Конструктор с параметром для объектов перечисления, он всегда приватный
         * @param abbr
         */
        private DirectionEx(String abbr) {
            this.abbr = abbr;
        }

        /**
         * открытый метод, для получения поля с аббревиатурой
         * @return
         */
        public String getAbbr() {
            return abbr;
        }

        /**
         * абстрактный метод, который реализуют подклассы перечисления
         * @return
         */
        public abstract DirectionEx inverse();
    }

    public static void main(String[] args) {
        /**
         * Примеры работы с перечислениями
         */
        System.out.println("примеры работы с перечислениями");
        Direction direction = Direction.LEFT;
        //— name() — имя константы в виде строки
        System.out.println("имя константы в виде строки " + direction.name());
        //— ordinal() — порядок константы (соответствует порядку, в котором объвлены константы)
        System.out.println("порядок константы " + direction.ordinal());
        //— valueOf() — статический метод, позволяющий получить объект перечисления по классу и имени
        direction = Direction.valueOf("UP");
        System.out.println("получение объекта перечислениям по классу и имени " + direction);
        //— values() — статический метод для получения массива всех элементов enum-класса
        System.out.println("массив всех объектов перечисления " + Arrays.toString(Direction.values()));

        DirectionEx directionEx = DirectionEx.DOWN;
        System.out.println("используем реализованный метод интерфейса " + directionEx.getName());
        System.out.println("используем реализованный абстрактый метод inverse "
                + directionEx + " = " +  directionEx.inverse());
        System.out.println("использует открытый метод для получения аббревиатуры элемента перечисления, abbr "
                + directionEx + " = " + directionEx.getAbbr());

        hr();

    }

}
