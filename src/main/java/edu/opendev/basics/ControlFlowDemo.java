package edu.opendev.basics;

import java.util.Arrays;
import java.util.List;

import static edu.opendev.Main.hr;

/**
 * Created by ralex on 30.08.16.
 */
public class ControlFlowDemo {


    /**
     * Оператор for
     */
    public static void forStatement() {
        System.out.println("Примеры работы цикла for");

        //массив чисел
        int[] numbers = {41, 32, -3, 23, 2, 8, 7, -55, 0, 11};
        //классический цикл
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("i = " + numbers[i]);
        }
        //цикл по коллекции или массиву, появившийстя в java 5
        for (int i : numbers) {
            System.out.println("Count is: " + i);
        }

        Integer[] intNumbers = {41, 32, -3, 23, 2, 8, 7, -55, 0, 11};
        List<Integer> intList = Arrays.asList(intNumbers);

        for (int i = 0; i < intList.size(); i++) {
            System.out.println("element = " + intList.get(i));
        }

        for (Integer item : intList) {
            System.out.println("item = " + item);
        }

        //цикл через лябда-выражение, нововведение java 8
        intList.forEach(value -> System.out.println("Count is: " + value));
        //то же, в сокращенном виде
        intList.forEach(System.out::println);

    }

    /**
     * Оператор if
     */
    public static void ifStatement() {

    }

    /**
     * Оператор while
     */
    public static void whileStatement() {
        System.out.println("Примеры работы цикла while");

        /*The while statement continually executes a block of statements while a particular condition is true.
         Its syntax can be expressed as:

        while (expression) {
            statement(s)
        }

        The while statement evaluates expression, which must return a boolean value. If the expression evaluates
         to true, the while statement executes the statement(s) in the while block. The while statement continues
          testing the expression and executing its block until the expression evaluates to false. Using the while
           statement to print the values from 1 through 10 can be accomplished as in the following WhileDemo program:*/

        int count = 1;
        while (count < 11) {
            System.out.println("Count is: " + count);
            count++;
        }

        /*You can implement an infinite loop using the while statement as follows:

        while (true){
            // your code goes here
        }*/

        /*The Java programming language also provides a do-while statement, which can be expressed as follows:

        do {
            statement(s)
        } while (expression);
        The difference between do-while and while is that do-while evaluates its expression at the bottom of the loop
        instead of the top. Therefore, the statements within the do block are always executed at least once, as shown
         in the following DoWhileDemo program:*/

        count = 1;
        do {
            System.out.println("Count is: " + count);
            count++;
        } while (count < 11);

    }

    public static void main(String[] args) {
        hr();
        ControlFlowDemo.forStatement();
        hr();
        ControlFlowDemo.whileStatement();
        hr();
    }
}
