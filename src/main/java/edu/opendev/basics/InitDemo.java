package edu.opendev.basics;

/**
 * Created by ralex on 15.09.16.
 */
//TODO доделать пример
public class InitDemo {

    private int n = 10;
    private String str = "init str";

    {
        n = 50;
        str = "zzzzzzzzzz";
    }

    public InitDemo() {
        //this(0, "");
        System.out.println(n);
        System.out.println(str);
    }

    public InitDemo(int n, String str) {
        this.n = n;
        this.str = str;
    }

    public int getN() {
        return n;
    }

    public String getStr() {
        return str;
    }
}
