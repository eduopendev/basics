package edu.opendev.basics;

import edu.opendev.copy_objects.Author;
import edu.opendev.copy_objects.Book;
import edu.opendev.copy_objects.BookEx;

import static edu.opendev.Main.hr;

/**
 * Created by ralex on 28.09.16.
 */
public class ObjectDemo {

    public static void main(String[] args) {
        /**
         * примеры переопределенных методов Object
         */
        Book book = new Book("Три мушкетера", new Author("А. Дюма"));
        Book bookEx = new BookEx(book);
        Book bookEx2 = new BookEx(book);
        System.out.println("вывод объекта через toString() по-умолчанию");
        System.out.println(book);
        System.out.println("вывод объекта через переопределенный toString()");
        System.out.println(bookEx);
        System.out.println("хэш-код по умолчанию");
        System.out.println(book.hashCode());
        System.out.println("свой хэш-код");
        System.out.println(bookEx.hashCode());
        if(book.equals(bookEx)) {
            System.out.println("book is equal bookEx");
        } else {
            System.out.println("book is not equal bookEx");
        }
        if(bookEx.equals(book)) {
            System.out.println("bookEx is equal book");
        } else {
            System.out.println("bookEx is not equal book");
        }
        if(bookEx.equals(bookEx2)) {
            System.out.println("bookEx is equal bookEx2");
        } else {
            System.out.println("bookEx is not equal bookEx2");
        }
        hr();

        System.out.println("примеры хэш-кодов");
        String s1 = "str";
        String s2 = new String(s1);
        String s3 = "str";
        //хэши этих строк одинаковые, см.реализацию String::hashCode();
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s3.hashCode());
        StringBuilder sb1 = new StringBuilder(s1);
        StringBuilder sb2 = new StringBuilder(s1);
        //хэши бирдеров разные, не смотря на то, что они содержать одну и ту же строку
        //т.к. для данного класса метод hashCode() не переопределен, и используется нативный код из Object
        System.out.println(sb1.hashCode());
        System.out.println(sb2.hashCode());
        hr();
    }

}
