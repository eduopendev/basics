package edu.opendev.reflection;

import edu.opendev.classtypes.OuterClassDemo;
import edu.opendev.copy_objects.*;
import edu.opendev.patterns.SingletonDemo;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashMap;

import static edu.opendev.Main.hr;

/**
 * Created by ralex on 21.09.16.
 */
public class ReflectionDemo {

    public int nPubField;
    public Book pubBook;
    private int nPrivField;

    public static void getClassDemo() throws ClassNotFoundException {
        //получение имени класса по известному классу
        Class aClass = BookEx.class;
        System.out.println(aClass.getName());
        System.out.println(aClass.getSimpleName());

        //получение имени класса через объект
        System.out.println(SingletonDemo.getInstance().getClass().getName());

        //получение имени класса по строковому значению имени
        System.out.println(Class.forName("edu.opendev.basics.ArrayDemo").getName());

        System.out.println();
    }

    public static void getModifiersDemo() {
        //получение модификатора класса или интерфейса в виде кода
        System.out.println("определяем модификатор класса или интерфейса");
        int mods = BookEx.class.getModifiers();
        //дешифровка полученного кода
        if (Modifier.isPublic(mods)) {
            System.out.println("public");
        }
        if (Modifier.isAbstract(mods)) {
            System.out.println("abstract");
        }
        if (Modifier.isFinal(mods)) {
            System.out.println("final");
        }
        System.out.println();
    }

    public static void getSuperclassesDemo() {
        //получаем суперклассы
        System.out.println("рекурсивно получаем суперклассы данного класса");
        Class parentClass = HashMap.class;//aClass;
        do {
            System.out.println(parentClass);
            Class[] interfaces = parentClass.getInterfaces();
            for (Class interf : interfaces) {
                System.out.println("интерфейс: " + interf.getName());
            }
            parentClass = parentClass.getSuperclass();
        } while (parentClass != null);

        System.out.println();
    }

    public static void getOuterClassDemo() {
        //получаем надкласс
        System.out.println("надкласс для вложенного класса");
        Class aClass = OuterClassDemo.StaticNestedClassDemo.class;
        System.out.println("getEnclosingClass() => " + aClass.getEnclosingClass());
        System.out.println("getDeclaringClass() => " + aClass.getDeclaringClass());
        System.out.println("getName() => " + aClass.getName());

        System.out.println();

        System.out.println("надкласс для внутреннего класса");
        aClass = OuterClassDemo.InnerClassDemo.class;
        System.out.println("getEnclosingClass() => " + aClass.getEnclosingClass());
        System.out.println("getDeclaringClass() => " + aClass.getDeclaringClass());
        System.out.println("getName() => " + aClass.getName());

        System.out.println();

        System.out.println("для анонимного класса");
        new Object() {
            void test() {
                Class aClass = this.getClass();
                System.out.println("getEnclosingClass() => " + aClass.getEnclosingClass());
                System.out.println("getDeclaringClass() => " + aClass.getDeclaringClass());
                System.out.println("getName() => " + aClass.getName());
            }
        }.test();

        System.out.println();

        System.out.println("для локального класса");
        {
            class LocalClass {
                void test() {
                    System.out.println("getEnclosingClass() => " + this.getClass().getEnclosingClass());
                    System.out.println("getDeclaringClass() => " + this.getClass().getDeclaringClass());
                    System.out.println("getName() => " + this.getClass().getName());
                }
            }
            LocalClass instanceOfLocalClass = new LocalClass();
            instanceOfLocalClass.test();
        }
        class LocalClass {
            void test() {
                System.out.println("getEnclosingClass() => " + this.getClass().getEnclosingClass());
                System.out.println("getDeclaringClass() => " + this.getClass().getDeclaringClass());
                System.out.println("getName() => " + this.getClass().getName());
            }
        }
        LocalClass instanceOfLocalClass = new LocalClass();
        instanceOfLocalClass.test();

        System.out.println();
    }

    public static void getInterfacesDemo() {
        Class aClass = ArrayList.class;
        System.out.println("получаем интерфейсы, реализуемые указанным классом (" + aClass.getSimpleName() + ")");
        Class[] interfaces = aClass.getInterfaces();
        for (Class cInterface : interfaces) {
            System.out.println(cInterface.getName());
        }

        System.out.println();
    }

    public static void getConstructorsDemo() {
        Class aClass = ArrayList.class;
        System.out.println("получаем открытые конструкторы указанного класса (" + aClass.getSimpleName() + ")");
        Constructor[] constructors = aClass.getConstructors();//чтобы получить все нужно исп. getDeclaredConstructors()
        for (Constructor constructor : constructors) {
            System.out.println(constructor.toGenericString());
        }

        System.out.println();
    }

    public static void getFieldsDemo() {
        System.out.println("получаем открытые поля класса");
        Class aClass = ReflectionDemo.class;
        Field[] publicFields = aClass.getFields();
        for (Field field : publicFields) {
            Class fieldType = field.getType();
            System.out.println("Имя: " + field.getName());
            System.out.println("Тип: " + fieldType.getName());
        }

        System.out.println();
        //aClass = Book.class;
        System.out.println("получаем все поля класса");
        Field[] allFields = aClass.getDeclaredFields();
        for (Field field : allFields) {
            Class fieldType = field.getType();
            System.out.println("Имя: " + field.getName());
            System.out.println("Тип: " + fieldType.getName());
        }

        System.out.println();
    }

    public static void getAndSetFieldValueDemo() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("получаем приватное поле и его значение, затем меняем его");
        Book bookEx = new BookEx("Три мушкетера", new Author("А.Дюма"));
        System.out.println(bookEx);
        Class aClass = bookEx.getClass().getSuperclass();
        Field field = aClass.getDeclaredField("title");
        field.setAccessible(true);
        String title = (String) field.get(bookEx);
        System.out.println(title);
        field.set(bookEx, "20 лет спустя");
        System.out.println(bookEx);

        System.out.println();
    }

    public static void getMethodsDemo() {
        System.out.println("получаем открытые методы класса");//чтобы получить все нужно исп. getDeclaredMethods()
        Class aClass = Book.class;
        Method[] methods = aClass.getMethods();

        for (Method method : methods) {
            System.out.println("Имя: " + method.getName());
            System.out.println("Возвращаемый тип: " + method.getReturnType().getName());

            Class[] paramTypes = method.getParameterTypes();
            System.out.print("Типы параметров: ");
            for (Class paramType : paramTypes) {
                System.out.print(" " + paramType.getName());
            }
            System.out.println();
            System.out.println("----------------");
        }

        System.out.println();
    }

    public static void invokeMethodDemo() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Book bookEx = new BookEx("Три мушкетера", new Author("А.Дюма"));
        System.out.println("получаем открытый метод, выполняем его");
        System.out.println(bookEx.getTitle());
        //Class bClass = bookEx.getClass();
        Class<?> bClass = bookEx.getClass();
        Class[] paramTypes = new Class[]{String.class};
        Method method = bClass.getMethod("setTitle", paramTypes);
        Object[] args = new Object[]{"New title"};

        method.invoke(bookEx, args);
       // bookEx.setTitle("setTitle");

        System.out.println(bookEx.getTitle());

        System.out.println("получаем приватный метод, выполняем его");
        paramTypes = new Class[]{String.class, int.class};
        method = bClass.getDeclaredMethod("testPrivateMethod", paramTypes);
        method.setAccessible(true);
        args = new Object[]{"я внутри защищенного кода ", 503};
        method.invoke(bookEx, args);

        System.out.println();
    }

    public static void newInstanceDemo() throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        System.out.println("динамическое создание экземпляра класса");
        System.out.println("через конструктор по-умолчанию");

        Class aClass = SerialObjDemo.class;
        Object object = aClass.newInstance();
        System.out.println(object);
        System.out.println("через параметризованный конструктор");
        aClass = Class.forName("edu.opendev.copy_objects.BookEx");
        Class[] constrParams = new Class[]{String.class, Author.class};
        Constructor constructor = aClass.getConstructor(constrParams);
        object = constructor.newInstance("test", new Author("test"));
        System.out.println(object);

        System.out.println();
    }

    public static void changePrivateStaticFinalFieldValue() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("изменяем конечное статическое поле");
        Class aClass = SerialObjWithStatic.class;
        Field field = aClass.getDeclaredField("strFinalStatic");
        field.setAccessible(true);
        System.out.println(field.get(null));
        field.setAccessible(false);


        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        //меняем модификатор поля field, исключая из него final
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.PRIVATE &~Modifier.FINAL ^ Modifier.PUBLIC);
        field.set(null, "новое значение конечного поля");
        System.out.println(field.get(null));
    }

    public static void main(String[] args) {
        /**
         * примеры с рефлексией
         */
        System.out.println("примеры с рефлексией");
        try {

            ReflectionDemo.getClassDemo();
            ReflectionDemo.getModifiersDemo();
            ReflectionDemo.getSuperclassesDemo();
            ReflectionDemo.getOuterClassDemo();
            ReflectionDemo.getInterfacesDemo();
            ReflectionDemo.getConstructorsDemo();
            ReflectionDemo.getFieldsDemo();
            ReflectionDemo.getAndSetFieldValueDemo();
            ReflectionDemo.getMethodsDemo();
            ReflectionDemo.invokeMethodDemo();
            ReflectionDemo.newInstanceDemo();
            ReflectionDemo.changePrivateStaticFinalFieldValue();

            System.out.println();
            System.out.println("пример от Хостманна");
            ObjectAnalyzer analyzer = new ObjectAnalyzer();

            ArrayList<Integer> squares = new ArrayList<>();
            for (int i = 1; i <= 5; i++) {
                squares.add(i * i);
            }
            System.out.println(analyzer.toString(squares));

            hr();
        } catch (NoSuchFieldException | InstantiationException | ClassNotFoundException | InvocationTargetException
                | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

}
