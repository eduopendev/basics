package edu.opendev.reflection.proxy;

import edu.opendev.patterns.decorator.coffee.Coffee;
import edu.opendev.patterns.decorator.coffee.SimpleCoffee;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

import static edu.opendev.patterns.decorator.coffee.CoffeeDemo.printInfo;

/**
 * Created by ralex on 08.11.16.
 */
public class ProxyDemo {

    public static void main(String[] args) {

        Coffee coffee = new SimpleCoffee();

        /*Coffee coffeeProxy = (Coffee) Proxy.newProxyInstance(coffee.getClass().getClassLoader()
                , new Class[]{Coffee.class}
                , new SpyProxyHandler(coffee));*/

        Coffee coffeeProxy = (Coffee) Proxy.newProxyInstance(coffee.getClass().getClassLoader(),
                coffee.getClass().getInterfaces(),
                new SpyProxyHandler(coffee));
        printInfo(coffeeProxy);

        Person person = new Person("Иван");
        IPerson personProxy = (IPerson) Proxy.newProxyInstance(person.getClass().getClassLoader()
                , person.getClass().getInterfaces()
                , new SpyProxyHandler(person));
        personProxy.getName();
        personProxy.setName("Степан");


    }

}

class SpyProxyHandler implements InvocationHandler {

    private final Object obj;

    public SpyProxyHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("я все вижу! вызван метод " + method.getName());
        System.out.println("аргументы: " + Arrays.toString(args));
        return method.invoke(obj, args);
    }

}

interface IPerson {
    String getName();

    void setName(String name);
}


class Person implements IPerson {

    private String name;

    Person(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
