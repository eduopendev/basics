package edu.opendev;


import edu.opendev.basics.*;
import edu.opendev.classtypes.OuterClassDemo;
import edu.opendev.copy_objects.SerialObjDemo;
import edu.opendev.db.JDBCDemo;
import edu.opendev.exceptions.ChainExcDemo;
import edu.opendev.generics.GenDemo;
import edu.opendev.patterns.SingletonDemo;
import edu.opendev.reflection.ReflectionDemo;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;


/**
 * типы данных
 * арифметические операции
 * операции отношения
 * битовые операции
 * классы Math и StrictMath
 * приведение типов
 */

public class Main {

    /**
     * статичное и неизменяемое поле, по-сути - именованная константа
     */
    final static double pi = 3.14;//3.14D, 3.14F

    /**
     * Точка входа в приложение
     *
     * @param args параметры запуска
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {

        /**
         * Примеры
         * Каждый пример можно запустить как отдельную программу через собственный main метод
         * См. комментарии в классах примеров
         */
        WelcomeDemo.main(null);
        ControlFlowDemo.main(null);
        SwitchStatementDemo.main(null);
        ArrayDemo.main(null);
        EnumDemo.main(null);
        IODemo.main(null);
        OuterClassDemo.main(null);
        ObjectDemo.main(null);
        SerialObjDemo.main(null);
        ReflectionDemo.main(null);
        ChainExcDemo.main(null);
        GenDemo.main(null);

        /**
         * пример паттерна "одиночка" (singleton)
         */
        SingletonDemo singleton = SingletonDemo.getInstance();
        singleton.makeSomethig();
        hr();


        JDBCDemo.demoQuery();
        JDBCDemo.test();

        List<String> list = Collections.synchronizedList(new ArrayList<>());


    }

    public static void hr() {
        System.out.println("---------------------------------------------------------");
    }
 //new change


}

