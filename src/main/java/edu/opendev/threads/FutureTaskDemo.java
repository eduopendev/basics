package edu.opendev.threads;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Created by ralex on 01.11.16.
 */
public class FutureTaskDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Integer> myComputation = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int sum = 0;
                for (int i = 0; i < 100; i++) {
                    sum += i;
                    Thread.sleep(10);
                }
                return sum;
            }
        };
        FutureTask<Integer> futureTask = new FutureTask<Integer>(myComputation);
        Thread t = new Thread(futureTask);  // это Runnable
        t.start();

        System.out.println("ждем результятов вычислений");

        Integer result = futureTask.get();  // это Future
        System.out.println(result);

    }

}
