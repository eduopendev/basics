package edu.opendev.threads.rwLock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ralex on 25.10.16.
 */
public class RWLockDemo {

    public static void main(String[] args) {
        final int threadCount = 2;
        final ExecutorService exService = Executors.newFixedThreadPool(threadCount);
        final ScoreBoard scoreBoard = new ScoreBoard();
        exService.execute(new ScoreUpdateThread(scoreBoard));
        exService.execute(new ScoreHealthThread(scoreBoard));
        exService.shutdown();
    }

}
