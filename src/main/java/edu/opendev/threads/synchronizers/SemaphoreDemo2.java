package edu.opendev.threads.synchronizers;

import java.util.concurrent.Semaphore;

/**
 * Еще один пример с семафором. Здесь есть общий ресурс CommonResource с полем x, которое изменяется каждым потоком.
 * Потоки представлены классом CountThread, который получает семафор и выполняет некоторые действия над ресурсом.
 * Created by ralex on 03.11.16.
 */
public class SemaphoreDemo2 {

    public static void main(String[] args) {
        Semaphore sem = new Semaphore(1); // 1 разрешение
        CommonResource res = new CommonResource();
        new Thread(new CountThread(res, sem, "CountThread 1")).start();
        new Thread(new CountThread(res, sem, "CountThread 2")).start();
        new Thread(new CountThread(res, sem, "CountThread 3")).start();
    }

}

class CommonResource {

    int x = 0;
}

class CountThread implements Runnable {

    CommonResource res;
    Semaphore sem;
    String name;

    CountThread(CommonResource res, Semaphore sem, String name) {
        this.res = res;
        this.sem = sem;
        this.name = name;
    }

    public void run() {

        try {
            System.out.println(name + " ожидает разрешение");
            sem.acquire();
            res.x = 1;
            for (int i = 1; i < 5; i++) {
                System.out.println(this.name + ": " + res.x);
                res.x++;
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(name + " освобождает разрешение");
        sem.release();
    }
}
