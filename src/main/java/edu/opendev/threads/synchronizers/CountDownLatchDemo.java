package edu.opendev.threads.synchronizers;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * В этом примере "защелки" CountDownLatch служит двум целям. Во-первых, одна защелка освобождает все потоки
 * одновременно, имитируя старт скачек, однако позднее другая защелка имитирует окончание гонки, чтобы основной поток
 * мог вывести на экран результаты. Чтобы добавить в "скачки" большее количество комментариев, можно добавить защелки
 * CountDownLatch на промежуточных этапах, когда "лошади" проходят четверть, половину и три четверти дистанции.
 * Created by ralex on 03.11.16.
 */
public class CountDownLatchDemo {

    public static void main(String[] args) throws IOException, InterruptedException {

        System.out.println("Prepping...");

        Race r = new Race(
                "Белка",
                "Конь-байкер",
                "Феликс",
                "Стрелка",
                "Бобик",
                "Череп",
                "Хвост"
        );

        System.out.println("It's a race of " + r.getDistance() + " lengths");

        System.out.println("Press Enter to run the race....");
        System.in.read();

        r.run();

    }

}

class Race {
    private Random rand = new Random();

    private int distance = 100;//rand.nextInt(250);
    private CountDownLatch start;
    private CountDownLatch finish;

    private List<String> horses = new ArrayList<String>();

    public Race(String... names) {
        this.horses.addAll(Arrays.asList(names));
    }

    public int getDistance() {
        return distance;
    }

    public void run()
            throws InterruptedException {
        System.out.println("And the horses are stepping up to the gate...");
        /*final CountDownLatch*/ start = new CountDownLatch(1);
        /*final CountDownLatch*/ finish = new CountDownLatch(horses.size());
        final List<String> places =
                Collections.synchronizedList(new ArrayList<String>());

        for (final String h : horses) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println(h +
                                " stepping up to the gate...");
                        start.await();

                        int traveled = 0;
                        while (traveled < distance) {
                            // через 0-2 секунды....
                            Thread.sleep(rand.nextInt(3) * 1000);

                            // ... лошадь проходит дистанцию 0-14 пунктов
                            traveled += rand.nextInt(15);
                            System.out.println(h +
                                    " advanced to " + traveled + "!");
                        }
                        finish.countDown();
                        System.out.println(h +
                                " crossed the finish!");
                        places.add(h);
                    } catch (InterruptedException intEx) {
                        System.out.println("ABORTING RACE!!!");
                        intEx.printStackTrace();
                    }
                }
            }).start();
        }

        System.out.println("And... they're off!");
        start.countDown();

        finish.await();
        System.out.println("And we have our winners!");
        System.out.println(places.get(0) + " took the gold...");
        System.out.println(places.get(1) + " got the silver...");
        System.out.println("and " + places.get(2) + " took home the bronze.");
    }
}
