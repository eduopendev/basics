package edu.opendev.threads.synchronizers;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * В этом примере выполняется 10 потоков (в чем можно убедиться, выполнив дамп для процесса), но только три из них
 * являются активными. Остальные семь вынуждены ждать, пока какой-нибудь из выполняющихся потоков не освободит семафор.
 * В итоге, все десят потоков выполнят свои задачи, но одновремменно будут выполняться только три.
 * Created by ralex on 03.11.16.
 */
public class SemaphoreDemo1 {

    public static void main(String[] args) {

        Runnable limitedCall = new Runnable() {
            final Random rand = new Random();
            final Semaphore available = new Semaphore(3);
            int count = 0;

            public void run() {
                int time = rand.nextInt(15);
                int num = count++;

                try {
                    available.acquire();

                    System.out.println("Executing " +
                            "long-running action for " +
                            time + " seconds... #" + num);

                    Thread.sleep(time * 1000);

                    System.out.println("Done with #" +
                            num + "!");

                    available.release();
                } catch (InterruptedException intEx) {
                    intEx.printStackTrace();
                }
            }
        };

        for (int i = 0; i < 10; i++) {
            new Thread(limitedCall).start();
        }

    }

}

