package edu.opendev.threads.duelBot;

import edu.opendev.threads.duelBot.actions.ActionDefence;
import edu.opendev.threads.duelBot.actions.ActionHit;
import edu.opendev.threads.duelBot.actions.ActionRage;
import edu.opendev.threads.duelBot.units.Axeman;
import edu.opendev.threads.duelBot.units.Swordsman;

/**
 * Created by Gratus on 28.10.2016.
 */
public class DuelDemo {

    public static void main(String[] args) {

        Axeman axeman = new Axeman("Освальд");
        Swordsman swordsman = new Swordsman("Брайан");

        /*while (axeman.getHealth() > 0 && swordsman.getHealth() > 0 ) {
            axeman.doAction(new ActionHit(axeman), swordsman);
            swordsman.doAction(new ActionHit(swordsman), axeman);
        }*/

        Thread thread1 = new Thread(new DuelRunnable(axeman, swordsman));
        Thread thread2 = new Thread(new DuelRunnable(swordsman, axeman));

        thread1.start();
        thread2.start();

    }

    public static void printMessage(String message) {
        System.out.println(message);
    }

}
