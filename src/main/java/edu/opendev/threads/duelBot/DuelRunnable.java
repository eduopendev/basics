package edu.opendev.threads.duelBot;

import edu.opendev.threads.duelBot.actions.ActionHit;
import edu.opendev.threads.duelBot.units.Unit;

/**
 * Created by Gratus on 28.10.2016.
 */
public class DuelRunnable implements Runnable {

    private final Unit attacker;
    private final Unit defender;
    private long delay;

    public DuelRunnable(Unit attacker, Unit defender) {
        this.attacker = attacker;
        this.defender = defender;
        this.delay = 60 * 1000 / attacker.getSpeed();
    }

    @Override
    public void run() {
        try {
            while (attacker.isAlive() && defender.isAlive()) {
                attacker.doAction(new ActionHit(attacker), defender);
                Thread.sleep(delay);
            }
            if(attacker.isAlive() && !defender.isAlive()) {
                DuelDemo.printMessage(String.format("%s погибает, побеждает %s", defender.getName(), attacker.getName()));
            }
        } catch (InterruptedException ignored) {
        }
    }
}
