package edu.opendev.threads.duelBot.actions;

import edu.opendev.threads.duelBot.DuelDemo;
import edu.opendev.threads.duelBot.units.Unit;

/**
 * Created by Gratus on 28.10.2016.
 */
public class ActionRage implements Action {
    @Override
    public void execute(Unit target) {
        target.incAttack(10);
        DuelDemo.printMessage(target.getName() + " вошел в раж");
    }
}
