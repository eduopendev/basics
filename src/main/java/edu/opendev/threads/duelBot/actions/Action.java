package edu.opendev.threads.duelBot.actions;

import edu.opendev.threads.duelBot.units.Unit;

/**
 * Created by Gratus on 28.10.2016.
 */
public interface Action {

    void execute(Unit target);

}
