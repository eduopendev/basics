package edu.opendev.threads.duelBot.actions;

import edu.opendev.threads.duelBot.DuelDemo;
import edu.opendev.threads.duelBot.units.Unit;

import java.text.Format;
import java.util.Formatter;
import java.util.Random;

/**
 * Created by Gratus on 28.10.2016.
 */
public class ActionHit implements Action {

    private final Unit actor;
    private final static Random rnd = new Random();

    public ActionHit(Unit actor) {
        this.actor = actor;
    }

    @Override
    public void execute(Unit target) {
        int damage = rnd.nextInt(actor.getAttack()) + 1;
        int absorb = rnd.nextInt(target.getDefence()) + 1;
        int finalDamage = damage > absorb ? damage - absorb : 0;
        String message;
        if(finalDamage > 0) {
            target.decHealth(finalDamage);
            message = String.format("%s нанес %d урона по %s", getNameWithHP(actor), finalDamage, getNameWithHP(target));
        } else {
            message = String.format("%s атаковал, но %s отразил удар", getNameWithHP(actor), getNameWithHP(target));
        }
        DuelDemo.printMessage(message);
    }

    private String getNameWithHP(Unit unit) {
        String title = String.format("%s[%d]", unit.getName(), unit.getHealth());
        return title;
    }

}
