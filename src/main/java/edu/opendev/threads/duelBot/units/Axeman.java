package edu.opendev.threads.duelBot.units;

import edu.opendev.threads.duelBot.actions.Action;
import edu.opendev.threads.duelBot.actions.ActionRage;

import java.util.Random;

/**
 * Created by Gratus on 28.10.2016.
 */
public class Axeman extends Unit {

    private final static Random rnd = new Random();

    public Axeman(String name) {
        super(name);
        attack = 30;
        defence = 20;
        speed = 55;
    }

    @Override
    public void decHealth(int delta) {
        super.decHealth(delta);
        if (isAlive() && rnd.nextInt(100) > 70) {
            this.doAction(new ActionRage(), this);
        }
    }
}
