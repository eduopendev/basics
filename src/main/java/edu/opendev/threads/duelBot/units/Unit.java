package edu.opendev.threads.duelBot.units;

import edu.opendev.threads.duelBot.actions.Action;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Gratus on 28.10.2016.
 */
public abstract class Unit {
    protected String name;
    protected int health = 30;
    protected int attack;
    protected int defence;
    protected int speed;//среднее количество действий в минуту
    protected Lock lock = new ReentrantLock();

    public Unit(String name) {
        this.name = name;
    }

    public void doAction(Action action, Unit target) {
        if(lock.tryLock()) {
            try {
                action.execute(target);
            } finally {
                lock.unlock();
            }
        }
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public int getSpeed() {
        return speed;
    }

    public void decHealth(int delta) {
        health = health < delta ? 0 : health - delta;
    }

    public void incDefence(int delta) {
        defence += delta;
    }

    public void incAttack(int delta) {
        attack += delta;
    }

    public boolean isAlive() {
        return health > 0;
    }
}
