package edu.opendev.threads;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by ralex on 25.10.16.
 */
public class DeadLockDemo2 {

    static class Friend {
        private final String name;
        private Lock lockObj = new ReentrantLock();

        public Friend(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public void bow(DeadLockDemo2.Friend bower) {
            lockObj.lock();
            System.out.format("%s: %s поклонился мне!%n", this.name, bower.getName());
            bower.bowBack(this);
            lockObj.unlock();
        }

        public void bowBack(DeadLockDemo2.Friend bower) {
            lockObj.lock();
            System.out.format("%s: %s поклонился мне в ответ!%n", this.name, bower.getName());
            lockObj.unlock();
        }
    }

    public static void main(String[] args) {
        final DeadLockDemo2.Friend james = new DeadLockDemo2.Friend("сэр Джеймс");
        final DeadLockDemo2.Friend luis = new DeadLockDemo2.Friend("сэр Льюис");

        new Thread(new Runnable() {
            public void run() {
                james.bow(luis);
            }
        }, "Джеймс").start();

        /*try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        new Thread(new Runnable() {
            public void run() {
                luis.bow(james);
            }
        }, "Льюис").start();
    }

}
