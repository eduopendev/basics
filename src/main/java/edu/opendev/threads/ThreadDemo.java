package edu.opendev.threads;

import edu.opendev.exceptions.MyUncheckedExc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Random;

/**
 * Created by ralex on 05.10.16.
 */
public class ThreadDemo {

    public static void main(String[] args) throws InterruptedException {
        exceptionExample();
        debaterExample();
    }

    public static void exceptionExample() {
        //метод задает обработчик по-умолчанию для необработанных (непойманных) исключений для всех потоков
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.err.printf("в потоке %s произошла ошибка %s", t.getName(), e.getMessage());
                System.err.println();
            }
        });

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FileInputStream fileInputStream = new FileInputStream("gradle.properties2");
                } catch (FileNotFoundException e) {
                    //бросаем свое непроверяемое исключение с проверяемым в качестве причины
                    throw new MyUncheckedExc(e.getMessage(), e);
                }
            }
        });
        //метод задает обработчик необработанных исключения для конкретного потока
        t1.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.err.println(t.getName() + e.toString());
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                //бросаем непроверяемое исключение
                throw new NullPointerException("тест");
            }
        });

        t1.start();//тут сработает обработчик заданный для этого конкретного потока
        t2.start();//тут сработает обработчик по-умолчанию для всех потоков
    }

    /**
     * запускаем побочный поток, который должен "переспорить" основной)
     * @throws InterruptedException
     */
    public static void debaterExample() throws InterruptedException {

        Debater debaterBot = new Debater();

        System.out.println("имя главного потока: " + Thread.currentThread().getName());
        System.out.println("имя вторичного потока: " + debaterBot.getName());

        System.out.println("Баттл начинается ...");
        debaterBot.start();

        int add = new Random().nextInt(5);
        for (int i = 0; i < 5; i++) {
            Thread.sleep(900+add);
            //System.out.println("[главный поток]: Я прав!");
            SimpleThreadsDemo.threadMessage("Я прав!");
        }

        if(debaterBot.isAlive()) {
            debaterBot.join();
            System.out.println("[побочный поток](победа): Я прав, и точка!!");
        } else {
            System.out.println("[главный поток](победа): Я же говорил, что прав Я!!!");
        }

    }

}
