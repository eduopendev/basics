package edu.opendev.threads;

/**
 * Created by ralex on 14.10.16.
 */
public class DeadLockDemo {

    static class Friend {
        private final String name;

        public Friend(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public synchronized void bow(Friend bower) {
            System.out.format("%s: %s поклонился мне!%n", this.name, bower.getName());
            bower.bowBack(this);
        }

        public synchronized void bowBack(Friend bower) {
            System.out.format("%s: %s поклонился мне в ответ!%n", this.name, bower.getName());
        }
    }

    public static void main(String[] args) {
        final Friend james = new Friend("сэр Джеймс");
        final Friend luis = new Friend("сэр Льюис");

        new Thread(new Runnable() {
            public void run() {
                james.bow(luis);
            }
        }).start();

        /*try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        new Thread(new Runnable() {
            public void run() {
                luis.bow(james);
            }
        }).start();
    }
}
