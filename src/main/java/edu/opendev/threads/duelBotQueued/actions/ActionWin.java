package edu.opendev.threads.duelBotQueued.actions;

import edu.opendev.threads.duelBot.DuelDemo;
import edu.opendev.threads.duelBotQueued.units.Unit;

/**
 * Created by ralex on 01.11.16.
 */
public class ActionWin extends Action {

    public ActionWin(Unit actor, Unit target) {
        super(actor, target);
    }

    @Override
    void execute() {
        if (actor.isAlive()) {
            DuelDemo.printMessage(String.format("%s погибает, побеждает %s", target.getName(),
                    actor.getName()));
        } else {
            DuelDemo.printMessage(String.format("%s погибает, побеждает %s", actor.getName(),
                    target.getName()));
        }
    }
}
