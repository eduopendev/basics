package edu.opendev.threads.duelBotQueued.actions;

import edu.opendev.threads.duelBotQueued.units.Unit;

/**
 * Created by ralex on 01.11.16.
 */
public abstract class Action {

    protected Unit actor;
    protected Unit target;

    public Action(Unit actor, Unit target) {
        this.actor = actor;
        this.target = target;
    }

    abstract void execute();

}
