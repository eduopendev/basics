package edu.opendev.threads.duelBotQueued.actions;

import edu.opendev.threads.duelBot.DuelDemo;
import edu.opendev.threads.duelBotQueued.units.Unit;

import java.util.Random;

/**
 * Created by ralex on 01.11.16.
 */
public class ActionHit extends Action {

    private final static Random rnd = new Random();

    public ActionHit(Unit actor, Unit target) {
        super(actor, target);
    }

    @Override
    public void execute() {
        int damage = rnd.nextInt(actor.getAttack()) + 1;
        int absorb = rnd.nextInt(target.getDefence()) + 1;
        int finalDamage = damage > absorb ? damage - absorb : 0;
        String message;
        if(finalDamage > 0) {
            target.decHealth(finalDamage);
            message = String.format("%s нанес %d урона по %s", getNameWithHP(actor), finalDamage, getNameWithHP(target));
        } else {
            message = String.format("%s атаковал, но %s отразил удар", getNameWithHP(actor), getNameWithHP(target));
        }
        DuelDemo.printMessage(message);
    }

    private String getNameWithHP(Unit unit) {
        String title = String.format("%s[%d]", unit.getName(), unit.getHealth());
        return title;
    }
}
