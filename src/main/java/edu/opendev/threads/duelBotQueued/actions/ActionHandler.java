package edu.opendev.threads.duelBotQueued.actions;

import edu.opendev.threads.duelBot.DuelDemo;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ralex on 01.11.16.
 */
public class ActionHandler {

    private final BlockingQueue<Action> queue = new ArrayBlockingQueue<Action>(100);

    private static ActionHandler instance = new ActionHandler();

    public static ActionHandler getInstance() {
        return instance;
    }

    public BlockingQueue<Action> getQueue() {
        return queue;
    }


    public void handle()  {
        try {
            Action action;
            do {
                action = queue.take();
                action.execute();
            } while (!(action instanceof ActionWin));
        } catch (InterruptedException e) {
            DuelDemo.printMessage(String.format("поток %s прерван", Thread.currentThread().getName()));
        }
    }

}
