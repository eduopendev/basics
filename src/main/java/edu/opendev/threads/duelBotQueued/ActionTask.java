package edu.opendev.threads.duelBotQueued;

import edu.opendev.threads.duelBot.DuelDemo;
import edu.opendev.threads.duelBotQueued.actions.Action;
import edu.opendev.threads.duelBotQueued.actions.ActionWin;
import edu.opendev.threads.duelBotQueued.actions.ActionHit;
import edu.opendev.threads.duelBotQueued.units.Unit;

import java.util.concurrent.BlockingQueue;

/**
 * Created by ralex on 01.11.16.
 */
public class ActionTask implements Runnable {

    private final Unit attacker;
    private final Unit defender;
    private final BlockingQueue<Action> queue;
    private final long delay;

    public ActionTask(Unit attacker, Unit defender, BlockingQueue<Action> queue) {
        this.attacker = attacker;
        this.defender = defender;
        this.queue = queue;
        this.delay = 60 * 1000 / attacker.getSpeed();
    }

    @Override
    public void run() {
        try {
            Action action = new ActionHit(attacker, defender);
            while (attacker.isAlive() && defender.isAlive()) {
                queue.put(action);
                Thread.sleep(delay);
            }
            queue.put(new ActionWin(attacker, defender));
        } catch (InterruptedException e) {
            DuelDemo.printMessage(String.format("поток %s прерван", Thread.currentThread().getName()));
        }
    }

}
