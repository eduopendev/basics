package edu.opendev.threads.duelBotQueued.units;

/**
 * Created by ralex on 01.11.16.
 */
public class Unit {

    protected String name;
    protected int health = 30;
    protected int attack;
    protected int defence;
    protected int speed;//среднее количество действий в минуту


    public Unit(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public int getSpeed() {
        return speed;
    }

    public void decHealth(int delta) {
        health = health < delta ? 0 : health - delta;
    }

    public void incDefence(int delta) {
        defence += delta;
    }

    public void incAttack(int delta) {
        attack += delta;
    }

    public boolean isAlive() {
        return health > 0;
    }

}
