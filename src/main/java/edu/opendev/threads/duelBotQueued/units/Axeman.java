package edu.opendev.threads.duelBotQueued.units;

/**
 * Created by ralex on 01.11.16.
 */
public class Axeman extends Unit {

    public Axeman(String name) {
        super(name);
        attack = 30;
        defence = 20;
        speed = 55;
    }

}
