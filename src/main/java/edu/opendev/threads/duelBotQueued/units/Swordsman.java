package edu.opendev.threads.duelBotQueued.units;

/**
 * Created by ralex on 01.11.16.
 */
public class Swordsman extends Unit {

    public Swordsman(String name) {
        super(name);
        attack = 25;
        defence = 25;
        speed = 60;
    }

}
