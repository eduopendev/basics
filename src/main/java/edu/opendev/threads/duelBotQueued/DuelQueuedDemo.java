package edu.opendev.threads.duelBotQueued;

import edu.opendev.threads.duelBotQueued.actions.Action;
import edu.opendev.threads.duelBotQueued.actions.ActionHandler;
import edu.opendev.threads.duelBotQueued.units.Axeman;
import edu.opendev.threads.duelBotQueued.units.Swordsman;
import edu.opendev.threads.duelBotQueued.units.Unit;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ralex on 01.11.16.
 */
public class DuelQueuedDemo {

    public static void main(String[] args) {

        BlockingQueue<Action> queue = ActionHandler.getInstance().getQueue();

        Unit swordsman = new Swordsman("Артур");
        Unit axeman = new Axeman("Ингвар");

        Thread t1 = new Thread(new ActionTask(swordsman, axeman, queue));
        Thread t2 = new Thread(new ActionTask(axeman, swordsman, queue));
        t1.start();
        t2.start();

        ActionHandler actionHandler = ActionHandler.getInstance();
        actionHandler.handle();

    }


}
