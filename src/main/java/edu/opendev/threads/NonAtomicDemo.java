package edu.opendev.threads;

/**
 * Created by ralex on 13.10.16.
 */
public class NonAtomicDemo {

    static int x;

    public static void main(String[] args) {
        /**
         * Инкремент - не атомарная операция
         * можно убедиться в этом, посмотрев байткод
         * javap -c -v NonAtomicDemo
         *   0: getstatic     #2 // Field x:I
             3: iconst_1
             4: iadd
             5: putstatic     #2 // Field x:I
             8: return
         */
        x++;
    }

}
