package edu.opendev.threads;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Пример состояния гонок между двумя потоками
 * Created by ralex on 13.10.16.
 */
public class SimpleRaceConditionDemo {

    //volatile отключает кэширвоание, для всех потоков будет видно одно значение переменной
    volatile static int x = 1;

    public static void main(String[] args) {

        // в первом потоке увеличиваем значение переменной
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!Thread.currentThread().isInterrupted()) {
                    x++;
                }
            }
        });

        // во втором потоке проверяем переменную на четность
        // и выводим сообщение, если она четная
        // из-за состояния гонок и не атомарности операций, увидим ситацию, когда проверка на четность прошла,
        // но в консоль выведено нечетное значение
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    if(x%2 == 0) {
                        System.out.println("x - четное, x = " + x);
                    }
                }
            }
        });

        t1.start();
        t2.start();

        // для прерывания поток используем таймер
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("прерываем потоки");
                t1.interrupt();
                t2.interrupt();
                timer.cancel();
            }
        }, 500);

        //вместо таймера можно было бы сделать и так
        /*try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t1.interrupt();
        t2.interrupt();*/

    }

}
