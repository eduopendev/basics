package edu.opendev.threads.horstmann.unsynch;

/**
 * This program shows data corruption when multiple threads access a data structure.
 * Программа демонстрирует повреждение данных при множественном доступе из разных потоков без синхронизации
 *
 * @author Cay Horstmann
 */
public class UnsynchBankDemo {
    /**
     * количество счетов для банка
     */
    public static final int NACCOUNTS = 100;
    /**
     * начальная сумма на счете
     */
    public static final double INITIAL_BALANCE = 1000;

    public static void main(String[] args) {
        //создаем банк
        Bank b = new Bank(NACCOUNTS, INITIAL_BALANCE);
        int i;
        //для каждого счета этого банка создаем поток, в котором производятся переводы с этого счета
        //на другой случайный счет некоторой случайной же суммы, потоки выполняются вечно
        //через некоторое время можно обнаружить, что общий баланс считается с ошибкой
        for (i = 0; i < NACCOUNTS; i++) {
            TransferRunnable r = new TransferRunnable(b, i, INITIAL_BALANCE);
            Thread t = new Thread(r);
            t.start();
        }
    }
}
