package edu.opendev.threads.horstmann.unsynch;

/**
 * Класс имитирует банк с заданым количеством счетов, на которых храняться некие суммы,
 * метод transfer переводит часть суммы с одного счета на другой
 * Общий баланс - сумма со всех счетов, должна оставаться постоянной
 *
 * @author Cay Horstmann
 * @version 1.30 2004-08-01
 */
public class Bank {

    /**
     * массив счетов в банке
     */
    private final double[] accounts;

    /**
     * Constructs the bank.
     *
     * @param n              the number of accounts
     * @param initialBalance the initial balance for each account
     */
    public Bank(int n, double initialBalance) {
        accounts = new double[n];
        for (int i = 0; i < accounts.length; i++)
            accounts[i] = initialBalance;
    }

    /**
     * Метод, перемещающий деньги с одного счета на другой
     * При вызовах из разных потоков метод не безопасен!
     * @param from   the account to transfer from
     * @param to     the account to transfer to
     * @param amount the amount to transfer
     */
    public synchronized void transfer(int from, int to, double amount) {
        if (accounts[from] < amount) return;//если на счете недостаточно средств для перевода, выходим
        System.out.print(Thread.currentThread());
        accounts[from] -= amount;//уменьшаем сумму с одного счета
        System.out.printf(" %10.2f from %d to %d", amount, from, to);
        accounts[to] += amount;//и прибавляем ее на другой
        System.out.printf(" Total Balance: %10.2f%n", getTotalBalance());
    }

    /**
     * Gets the sum of all account balances.
     *
     * @return the total balance
     */
    public double getTotalBalance() {
        double sum = 0;

        for (double a : accounts) {
            sum += a;
        }

        return sum;
    }

    /**
     * Gets the number of accounts in the bank.
     *
     * @return the number of accounts
     */
    public int size() {
        return accounts.length;
    }
}
