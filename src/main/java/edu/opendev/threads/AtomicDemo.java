package edu.opendev.threads;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * В пакете java.util.concurrent.atomic содержится ряд классов, гарантирующих атомарность тех или иных операций.
 * Атомарные классы гарантируют, что определённые операции будут выполняться потокобезопасно, например операции
 * инкремента и декремента, обновления и добавления(add) значения. Список атомных классов включает AtomicInteger,
 * AtomicBoolean, AtomicLong, AtomicIntegerArray, и так далее.
 * Created by ralex on 24.10.16.
 */
public class AtomicDemo {

    /**
     * Атомарное целое
     */
    static AtomicInteger atomicInteger = new AtomicInteger(10);

    public static void main(String[] args) {
        /**
         * После декомпиляции видим что байткод инкремента представлен одной операцией
         * javap -c -v AtomicDemo
         *   0: getstatic     #2 // Field atomicInteger:Ljava/util/concurrent/atomic/AtomicInteger;
             3: invokevirtual #3 // Method java/util/concurrent/atomic/AtomicInteger.incrementAndGet:()I
             6: pop
             7: return
         */
        atomicInteger.incrementAndGet();
    }

}
